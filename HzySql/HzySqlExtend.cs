﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql
{
    using HzySql.Interface;
    using HzySql.Models;
    using System.Reflection;

    public static class HzySqlExtend
    {

        /// <summary>
        /// case when then
        /// </summary>
        /// <param name="hzyTuple"></param>
        /// <returns></returns>
        public static CaseAchieve Case(this HzyTuple hzyTuple) => new CaseAchieve();

        #region Aop

        /// <summary>
        /// 缓存字段信息时回调
        /// </summary>
        public static Action<PropertyInfo, Models.FieldInfo, Type> CacheFieldInfoCall { get; set; }

        /// <summary>
        /// Insert 之前回调
        /// </summary>
        public static Action InsertBeforeCall { get; set; }

        /// <summary>
        /// Update 之前回调
        /// </summary>
        public static Action UpdateBeforeCall { get; set; }

        /// <summary>
        /// 执行Sql之前回调
        /// </summary>
        public static Action<ISqlContext, string> ExecuteSqlBeforeCall { get; set; }

        /// <summary>
        /// 异常回调
        /// </summary>
        public static Action<Exception> ExceptionCall { get; set; }

        #endregion

        #region defalut

        /// <summary>
        /// 连表默认连接对象
        /// </summary>
        /// <value></value>
        public static JoinType DefaultJoinType { get; set; } = JoinType.LEFT_JOIN;

        /// <summary>
        /// 如果对象 为 null 是 返回实例 还是 null => true=实例 false=null
        /// </summary>
        public static bool ReturnInstance = false;


        #endregion


    }
}
