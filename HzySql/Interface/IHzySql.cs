﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Interface
{
    using System.Data;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using HzySql.Curd;
    using HzySql.Curd.Select;
    using HzySql.Models;

    public interface IHzySql
    {
        IHzySqlAdoProvider AdoProvider { get; set; }
        ISqlContext Context { get; set; }

        #region Insert
        IInsert<T> Insert<T>(Expression<Func<T>> Entity) where T : class, new();
        IInsert<T> Insert<T>(T Entity) where T : class, new();
        #endregion

        #region Update
        IUpdate<T> Update<T>(Expression<Func<T>> Entity) where T : class, new();
        IUpdate<T> Update<T>(Expression<Func<T, T>> Entity) where T : class, new();
        IUpdate<T> Update<T>(T Entity) where T : class, new();
        IUpdate<T> UpdateById<T>(T Entity) where T : class, new();
        IUpdate<T> Update<T>(Expression<Func<T>> Entity, Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        IUpdate<T> Update<T>(Expression<Func<T, T>> Entity, Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        IUpdate<T> Update<T>(T Entity, Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        #endregion

        #region Delete
        IDelete<T> Delete<T>() where T : class, new();
        IDelete<T> DeleteById<T>(object Id) where T : class, new();
        IDelete<T> Delete<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        #endregion

        #region 批量执行 Batch Execute
        int ExecuteBatch(List<ISqlContext> ISqlCodeContextList);
        Task<int> ExecuteBatchAsync(List<ISqlContext> ISqlCodeContextList);
        #endregion

        #region Select
        Expression<Func<HzyTuple<T>, bool>> GetIdWhereByEntity<T>(T Entity) where T : class, new();
        Expression<Func<HzyTuple<T>, bool>> GetIdWhereById<T>(object Id) where T : class, new();
        IQuery<T> Query<T>() where T : class, new();
        IQuery<T> Query<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();

        #region 同步
        T Find<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        T FindById<T>(object Id) where T : class, new();
        List<T> FindList<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        List<T> FindList<T>(Expression<Func<HzyTuple<T>, bool>> Where, Expression<Func<HzyTuple<T>, object>> OrderBy) where T : class, new();
        #endregion
        #region 异步
        Task<T> FindAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        Task<T> FindByIdAsync<T>(object Id) where T : class, new();
        Task<List<T>> FindListAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        Task<List<T>> FindListAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where, Expression<Func<HzyTuple<T>, object>> OrderBy) where T : class, new();
        #endregion

        #endregion

        #region Aop 拦截器
        void UseAopCacheFieldInfoCall(Action<PropertyInfo, Models.FieldInfo, Type> call);
        void UseAopInsertBeforeCall(Action call);
        void UseAopUpdateBeforeCall(Action call);
        void UseAopExecuteSqlBeforeCall(Action<ISqlContext, string> call);
        void UseAopExceptionCall(Action<Exception> call);
        #endregion

    }

}
