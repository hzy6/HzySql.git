﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Interface
{
    using HzySql.Models;
    using System.Data;
    using System.Threading.Tasks;

    public interface IHzySqlAdoProvider
    {
        Func<IDbConnection> IDbConnectionCall { get; set; }
        string DbConnectionString { get; set; }
        IDbConnection DbConnection { get; set; }
        IDbTransaction DbTransaction { get; set; }
        bool CommitState { get; set; }
        int? CommandTimeout { get; set; }

        #region 同步方法

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        object ExecuteScalar(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        T ExecuteScalar<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 返回受影响行数
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        int Execute(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 单行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        T QueryFirstOrDefault<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 多行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        IEnumerable<T> Query<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 返回一个 datatable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        DataTable QueryTable(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 返回 IDataReader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        IDataReader ExecuteReader(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        #endregion

        #region 异步方法

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        Task<object> ExecuteScalarAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        Task<T> ExecuteScalarAsync<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 返回受影响行数
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        Task<int> ExecuteAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 单行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        Task<T> QueryFirstOrDefaultAsync<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 多行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> QueryAsync<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 返回一个 datatable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        Task<DataTable> QueryTableAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        /// <summary>
        /// 返回 IDataReader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        Task<IDataReader> ExecuteReaderAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null);

        #endregion

        #region 事务

        IDbTransaction BeginTransaction();

        void Commit();

        void Rollback();

        #endregion

    }
}
