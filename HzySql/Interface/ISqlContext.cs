﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Interface
{
    using HzySql.Models;
    using System.Data;
    using System.Threading.Tasks;

    public interface ISqlContext
    {
        IHzySqlAdoProvider AdoProvider { get; set; }
        bool IsAlias { get; set; }
        string Symbol(string Name);
        string ParametricSymbols { get; }
        List<DataParameter> Parameter { get; set; }
        Dictionary<string, string> Alias { get; set; }
        DataBaseType DataBaseType { get; set; }
        Func<ISqlContext> NewContextCall { get; set; }

        #region Sql 容器
        StringBuilder Code { get; set; }
        StringBuilder Cols { get; set; }
        StringBuilder From { get; set; }
        StringBuilder Join { get; set; }
        StringBuilder Where { get; set; }
        StringBuilder GroupBy { get; set; }
        StringBuilder Having { get; set; }
        StringBuilder OrderBy { get; set; }
        StringBuilder TakePage { get; set; }
        StringBuilder LockWith { get; set; }
        #endregion

        #region Insert、Update、Delete、Select
        void CreateInsertSql(string TableName, List<string> Cols, List<string> Values);
        void CreateUpdateSql(string TableName, List<string> Sets);
        void CreateDeleteSql(string TableName);
        ISqlContext ToSql();
        StringBuilder ToSql(StringBuilder Cols, StringBuilder From, StringBuilder Join, StringBuilder Where, StringBuilder GroupBy, StringBuilder Having, StringBuilder OrderBy, StringBuilder TakePage);
        string ToSqlString();
        void CreateTakePageSql(int Page, int Rows);
        void CreateFromSql<T>();
        void CreateSelectSql(LambdaExpression Lambda);
        void CreateWhereSql(LambdaExpression Lambda, string StartSymbol = "AND");
        void CreateGroupBySql(LambdaExpression Lambda);
        void CreateJoinSql(LambdaExpression Lambda, JoinType _JoinType);
        void CreateOrderBySql(LambdaExpression Lambda);
        void CreateOrderByDescSql(LambdaExpression Lambda);
        void CreateHavingSql(LambdaExpression Lambda);
        void CreateDistinctSql();
        void CreateTopSql(int Top);
        ISqlContext CreateUnion(ISqlContext Union, string Symbol = "Sub");
        void CreateMaxSql(LambdaExpression Lambda);
        void CreateMinSql(LambdaExpression Lambda);
        void CreateSumSql(LambdaExpression Lambda);
        StringBuilder CreateCountSql();
        void CreateLockWith(bool isLock = true);
        #endregion

        #region 数据转换

        #region 同步
        int ToCount();
        TR ToFirst<TR>();
        IEnumerable<TR> ToList<TR>();
        DataTable ToTable();
        TR ToMax<TR>(LambdaExpression Lambda);
        TR ToMin<TR>(LambdaExpression Lambda);
        TR ToSum<TR>(LambdaExpression Lambda);
        Dictionary<string, object> ToDictionary();
        List<Dictionary<string, object>> ToDictionaryList();
        #endregion

        #region 异步
        Task<int> ToCountAsync();
        Task<TR> ToFirstAsync<TR>();
        Task<IEnumerable<TR>> ToListAsync<TR>();
        Task<DataTable> ToTableAsync();
        Task<TR> ToMaxAsync<TR>(LambdaExpression Lambda);
        Task<TR> ToMinAsync<TR>(LambdaExpression Lambda);
        Task<TR> ToSumAsync<TR>(LambdaExpression Lambda);
        Task<Dictionary<string, object>> ToDictionaryAsync();
        Task<List<Dictionary<string, object>>> ToDictionaryListAsync();
        #endregion

        TR InsertToSave<TR>();
        Task<TR> InsertToSaveAsync<TR>();
        int InsertOrUpdateOrDeleteToSave();
        Task<int> InsertOrUpdateOrDeleteToSaveAsync();

        /// <summary>
        /// 检查Ado提供器
        /// </summary>
        void CheckAdoProviderNull();

        #endregion

        #region 获取用到的表集合

        List<string> GetTableNames();

        #endregion


    }






}
