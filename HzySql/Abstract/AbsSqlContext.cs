﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Abstract
{
    using HzySql.Interface;
    using HzySql.Models;
    using System.Data;
    using System.Threading.Tasks;

    public abstract class AbsSqlContext : ISqlContext
    {
        public abstract IHzySqlAdoProvider AdoProvider { get; set; }
        public abstract bool IsAlias { get; set; }
        public abstract string Symbol(string Name);
        public abstract string ParametricSymbols { get; }
        public abstract List<DataParameter> Parameter { get; set; }
        public abstract Dictionary<string, string> Alias { get; set; }
        public abstract DataBaseType DataBaseType { get; set; }
        public abstract Func<ISqlContext> NewContextCall { get; set; }

        #region Sql 容器
        public StringBuilder Code { get; set; } = new StringBuilder();
        public StringBuilder Cols { get; set; } = new StringBuilder();
        public StringBuilder From { get; set; } = new StringBuilder();
        public StringBuilder Join { get; set; } = new StringBuilder();
        public StringBuilder Where { get; set; } = new StringBuilder();
        public StringBuilder GroupBy { get; set; } = new StringBuilder();
        public StringBuilder Having { get; set; } = new StringBuilder();
        public StringBuilder OrderBy { get; set; } = new StringBuilder();
        public StringBuilder TakePage { get; set; } = new StringBuilder();
        public StringBuilder LockWith { get; set; } = new StringBuilder();
        #endregion

        #region Insert、Update、Delete、Select
        public abstract void CreateInsertSql(string TableName, List<string> Cols, List<string> Values);
        public abstract void CreateUpdateSql(string TableName, List<string> Sets);
        public abstract void CreateDeleteSql(string TableName);
        //
        public abstract ISqlContext ToSql();
        public abstract StringBuilder ToSql(StringBuilder Cols, StringBuilder From, StringBuilder Join, StringBuilder Where, StringBuilder GroupBy, StringBuilder Having, StringBuilder OrderBy, StringBuilder TakePage);
        public abstract string ToSqlString();
        public abstract void CreateTakePageSql(int Page, int Rows);
        public abstract void CreateFromSql<T>();
        public abstract void CreateSelectSql(LambdaExpression Lambda);
        public abstract void CreateWhereSql(LambdaExpression Lambda, string StartSymbol = "AND");
        public abstract void CreateGroupBySql(LambdaExpression Lambda);
        public abstract void CreateJoinSql(LambdaExpression Lambda, JoinType _JoinType);
        public abstract void CreateOrderBySql(LambdaExpression Lambda);
        public abstract void CreateOrderByDescSql(LambdaExpression Lambda);
        public abstract void CreateHavingSql(LambdaExpression Lambda);
        public abstract void CreateDistinctSql();
        public abstract void CreateTopSql(int Top);
        public abstract ISqlContext CreateUnion(ISqlContext Union, string Symbol);
        public abstract void CreateMaxSql(LambdaExpression Lambda);
        public abstract void CreateMinSql(LambdaExpression Lambda);
        public abstract void CreateSumSql(LambdaExpression Lambda);
        public abstract StringBuilder CreateCountSql();
        public abstract void CreateLockWith(bool isLock);
        #endregion

        #region 数据转换

        #region 同步
        public abstract int ToCount();
        public abstract TR ToFirst<TR>();
        public abstract IEnumerable<TR> ToList<TR>();
        public abstract DataTable ToTable();
        public abstract TR ToMax<TR>(LambdaExpression Lambda);
        public abstract TR ToMin<TR>(LambdaExpression Lambda);
        public abstract TR ToSum<TR>(LambdaExpression Lambda);
        public abstract Dictionary<string, object> ToDictionary();
        public abstract List<Dictionary<string, object>> ToDictionaryList();
        #endregion

        #region 异步
        public abstract Task<int> ToCountAsync();
        public abstract Task<TR> ToFirstAsync<TR>();
        public abstract Task<IEnumerable<TR>> ToListAsync<TR>();
        public abstract Task<DataTable> ToTableAsync();
        public abstract Task<TR> ToMaxAsync<TR>(LambdaExpression Lambda);
        public abstract Task<TR> ToMinAsync<TR>(LambdaExpression Lambda);
        public abstract Task<TR> ToSumAsync<TR>(LambdaExpression Lambda);
        public abstract Task<Dictionary<string, object>> ToDictionaryAsync();
        public abstract Task<List<Dictionary<string, object>>> ToDictionaryListAsync();
        #endregion

        public abstract TR InsertToSave<TR>();
        public abstract Task<TR> InsertToSaveAsync<TR>();
        public abstract int InsertOrUpdateOrDeleteToSave();
        public abstract Task<int> InsertOrUpdateOrDeleteToSaveAsync();

        //
        public abstract void CheckAdoProviderNull();

        #endregion

        #region 获取用到的表集合

        public abstract List<string> GetTableNames();

        #endregion


    }
}
