﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Abstract
{
    using HzySql.Curd;
    using HzySql.Models;
    using HzySql.Interface;
    using HzySql.Curd.Select;
    using System.Threading.Tasks;
    using System.Reflection;
    using System.Data;

    public abstract class AbsHzySql : IHzySql
    {
        public abstract IHzySqlAdoProvider AdoProvider { get; set; }
        public abstract ISqlContext Context { get; set; }

        #region Insert
        public abstract IInsert<T> Insert<T>(Expression<Func<T>> Entity) where T : class, new();
        public abstract IInsert<T> Insert<T>(T Entity) where T : class, new();
        #endregion

        #region Update
        public abstract IUpdate<T> Update<T>(Expression<Func<T>> Entity) where T : class, new();
        public abstract IUpdate<T> Update<T>(Expression<Func<T, T>> Entity) where T : class, new();
        public abstract IUpdate<T> Update<T>(T Entity) where T : class, new();
        public abstract IUpdate<T> UpdateById<T>(T Entity) where T : class, new();
        public abstract IUpdate<T> Update<T>(Expression<Func<T>> Entity, Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        public abstract IUpdate<T> Update<T>(Expression<Func<T, T>> Entity, Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        public abstract IUpdate<T> Update<T>(T Entity, Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        #endregion

        #region Delete
        public abstract IDelete<T> Delete<T>() where T : class, new();
        public abstract IDelete<T> Delete<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        public abstract IDelete<T> DeleteById<T>(object Id) where T : class, new();
        #endregion

        #region 批量执行 Batch Execute
        public abstract int ExecuteBatch(List<ISqlContext> ISqlCodeContextList);
        public abstract Task<int> ExecuteBatchAsync(List<ISqlContext> ISqlCodeContextList);
        #endregion

        #region Select
        public abstract Expression<Func<HzyTuple<T>, bool>> GetIdWhereByEntity<T>(T Entity) where T : class, new();
        public abstract Expression<Func<HzyTuple<T>, bool>> GetIdWhereById<T>(object Id) where T : class, new();
        public abstract IQuery<T> Query<T>() where T : class, new();
        public abstract IQuery<T> Query<T>(Expression<Func<HzyTuple<T>, bool>> Exp) where T : class, new();

        #region 同步
        public abstract T Find<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        public abstract T FindById<T>(object Id) where T : class, new();
        public abstract List<T> FindList<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        public abstract List<T> FindList<T>(Expression<Func<HzyTuple<T>, bool>> Where, Expression<Func<HzyTuple<T>, object>> OrderBy) where T : class, new();
        #endregion
        #region 异步
        public abstract Task<T> FindAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        public abstract Task<T> FindByIdAsync<T>(object Id) where T : class, new();
        public abstract Task<List<T>> FindListAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where) where T : class, new();
        public abstract Task<List<T>> FindListAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where, Expression<Func<HzyTuple<T>, object>> OrderBy) where T : class, new();
        #endregion

        #endregion

        #region Aop 拦截器
        public abstract void UseAopCacheFieldInfoCall(Action<PropertyInfo, Models.FieldInfo, Type> call);
        public abstract void UseAopInsertBeforeCall(Action call);
        public abstract void UseAopUpdateBeforeCall(Action call);
        public abstract void UseAopExecuteSqlBeforeCall(Action<ISqlContext, string> call);
        public abstract void UseAopExceptionCall(Action<Exception> call);
        #endregion

    }
}
