﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql
{
    public class HzySqlException : Exception
    {
        public HzySqlException(string Message)
         : base(Message)
        {

        }

        public HzySqlException(string Message, Exception InnerException)
            : base(Message, InnerException)
        {

        }


    }
}
