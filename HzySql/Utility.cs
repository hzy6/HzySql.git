﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql
{
    using HzySql.Models;
    using System.Data;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    public static class Utility
    {
        /// <summary>
        /// 获取 PropertyInfo 集合
        /// </summary>
        /// <param name="_type"></param>
        /// <param name="_bindingFlags"></param>
        /// <returns></returns>
        public static PropertyInfo[] GetPropertyInfos(Type _type, BindingFlags _bindingFlags = (BindingFlags.Instance | BindingFlags.Public)) => _type.GetProperties(_bindingFlags);

        /// <summary>
        /// 创建 对象实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T CreateInstance<T>()
        {
            var _Type = typeof(T);
            if (_Type.IsValueType || typeof(T) == typeof(string))
                return default(T);
            return (T)Activator.CreateInstance(_Type);
        }

        /// <summary>
        /// 获取 对象 中 某个属性得 标记
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_type"></param>
        /// <param name="_name"></param>
        /// <returns></returns>
        public static T GetAttribute<T>(Type _type, string _name)
            where T : Attribute
            => Utility.GetPropertyInfo(_type, _name).GetCustomAttribute(typeof(T)) as T;

        /// <summary>
        /// 获取 PropertyInfo 对象
        /// </summary>
        /// <param name="_type"></param>
        /// <param name="_name"></param>
        /// <returns></returns>
        public static PropertyInfo GetPropertyInfo(Type _type, string _name) => _type.GetProperty(_name);

        #region Expression 帮助函数

        /// <summary>
        /// Eval
        /// </summary>
        /// <param name="_Expression"></param>
        /// <returns></returns>
        public static object Eval(Expression _Expression)
        {
            var cast = Expression.Convert(_Expression, typeof(object));
            return Expression.Lambda<Func<object>>(cast).Compile().Invoke();
        }

        /// <summary>
        /// 根据实体对象 的 ID 创建 Expression<Func<HzyTuple<T>, bool>> 表达式树 例如： Lambda = ( w=>w.t1.Key==Guid.Empty )
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_KeyName"></param>
        /// <param name="_KeyValue"></param>
        /// <returns></returns>
        public static Expression<Func<HzyTuple<T>, bool>> IdWhere<T>(string _KeyName, object _KeyValue, string _ParName = "_Where_Parameter")
        {
            //创建 Where Lambda表达式树
            var _Type = typeof(HzyTuple<T>);
            var _Parmeter = Expression.Parameter(_Type, "w");
            var _Where_Parameter = Expression.Parameter(_Type, _ParName);
            var _Property = _Type.GetProperty(nameof(HzyTuple<T>.t1));
            //元组参数
            var _Left = Expression.Property(_Parmeter, _Property);
            //字段名
            var _KeyProperty = _Property.PropertyType.GetProperty(_KeyName);
            //w=>w.t1.Key
            var _NewLeft = Expression.Property(_Left, _KeyProperty);
            //==Guid.Empty
            var _Sign = _KeyValue == null;
            if (!_Sign) _Sign = (_KeyValue is string & string.IsNullOrWhiteSpace(_KeyValue.ToString()));
            if (_KeyProperty.PropertyType == typeof(Guid) & _Sign) _KeyValue = Guid.Empty;
            if (_KeyProperty.PropertyType == typeof(int) & _Sign) _KeyValue = Int32.MinValue;
            if (_KeyProperty.PropertyType == typeof(Guid) & !_Sign) _KeyValue = Guid.Parse(_KeyValue.ToString());
            if (_KeyProperty.PropertyType == typeof(int) & !_Sign) _KeyValue = Int32.Parse(_KeyValue.ToString());
            try
            {
                var _Where_Body = Expression.Equal(_NewLeft, Expression.Constant(_KeyValue, _KeyProperty.PropertyType));
                return Expression.Lambda<Func<HzyTuple<T>, bool>>(_Where_Body, _Where_Parameter);
            }
            catch (Exception ex)
            {
                if (_KeyProperty.PropertyType != _KeyValue.GetType())
                    throw new HzySqlException("请将主键值 转换为 正确的类型值！", ex);
                else
                    throw;
            }
        }

        /// <summary>
        /// 将 Model 转换为 MemberInitExpression 类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static MemberInitExpression ModelToMemberInitExpression<T>(T Model)
        {
            var proInfo = Utility.GetPropertyInfos(typeof(T));

            var list = new List<MemberBinding>();

            foreach (var item in proInfo) list.Add(Expression.Bind(item, Expression.Constant(item.GetValue(Model), item.PropertyType)));

            var newExpr = Expression.New(typeof(T));

            return Expression.MemberInit(newExpr, list);
        }

        #endregion

        #region IDataReader 转换

        /// <summary>
        /// IDataReader 转换 实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static T ToEntity<T>(this IDataReader _IDataReader) where T : class, new()
        {
            var _Entity = Utility.CreateInstance<T>();
            var propertyInfo = _Entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            if (propertyInfo.Length == 0) throw new HzySqlException("找不到任何 公共属性！");

            List<string> field = new List<string>(_IDataReader.FieldCount);
            for (int i = 0; i < _IDataReader.FieldCount; i++)
            {
                field.Add(_IDataReader.GetName(i));
            }
            var count = 0;
            while (_IDataReader.Read())
            {
                if (count > 0) break;
                foreach (var item in propertyInfo)
                {
                    string AttrName = item.Name;
                    if (!field.Contains(AttrName)) continue;
                    if (_IDataReader[AttrName] == DBNull.Value) continue;
                    item.SetValue(_Entity, _IDataReader[AttrName], null);
                }
                count++;
            }
            if (_IDataReader != null && !_IDataReader.IsClosed) _IDataReader.Close();
            return _Entity;
        }

        /// <summary>
        /// IDataReader 转换 ValueTuple<T1, T2>
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="idataReader"></param>
        /// <returns></returns>
        public static (T1, T2) ToTuple<T1, T2>(this IDataReader _IDataReader)
        {
            var tuple = new ValueTuple<T1, T2>();
            var count = 0;
            while (_IDataReader.Read())
            {
                if (count > 0) break;
                for (int i = 0; i < _IDataReader.FieldCount; i++)
                {
                    var value = _IDataReader[i];
                    if (value == DBNull.Value) continue;

                    switch (i + 1)
                    {
                        case 1:
                            tuple.Item1 = (T1)value;
                            break;
                        case 2:
                            tuple.Item2 = (T2)value;
                            break;
                        default:
                            break;
                    }
                }
                count++;
            }
            if (_IDataReader != null && !_IDataReader.IsClosed) _IDataReader.Close();
            return tuple;
        }

        /// <summary>
        /// 将 IDataReader 对象转换为 List<T>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_IDataReader"></param>
        /// <returns></returns>
        public static List<T> ToListByIDataReader<T>(this IDataReader _IDataReader)
        {
            if (_IDataReader.IsClosed) throw new HzySqlException("IDataReader 对象连接已关闭！");
            var res = new List<T>();
            var _Entity = Utility.CreateInstance<T>();
            var propertyInfo = _Entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            List<string> field = new List<string>(_IDataReader.FieldCount);
            for (int i = 0; i < _IDataReader.FieldCount; i++)
            {
                field.Add(_IDataReader.GetName(i));
            }

            while (_IDataReader.Read())
            {
                _Entity = Utility.CreateInstance<T>();
                foreach (var item in propertyInfo)
                {
                    string AttrName = item.Name;
                    if (!field.Contains(AttrName)) continue;
                    if (_IDataReader[AttrName] == DBNull.Value) continue;
                    item.SetValue(_Entity, _IDataReader[AttrName], null);
                }
                res.Add(_Entity);
            }
            if (_IDataReader != null && !_IDataReader.IsClosed) _IDataReader.Close();
            return res;
        }

        /// <summary>
        /// 将IDataReader对象转换为字典
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_IDataReader"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> ToDictionaryListByIDataReader(this IDataReader _IDataReader)
        {
            if (_IDataReader.IsClosed) throw new HzySqlException("IDataReader 对象连接已关闭！");
            var res = new List<Dictionary<string, object>>();

            while (_IDataReader.Read())
            {
                var dic = new Dictionary<string, object>();
                for (int i = 0; i < _IDataReader.FieldCount; i++)
                {
                    if (_IDataReader.IsDBNull(i))
                        dic.Add(_IDataReader.GetName(i), null);
                    else
                        dic.Add(_IDataReader.GetName(i), _IDataReader.GetValue(i));
                }
                res.Add(dic);
            }
            if (_IDataReader != null && !_IDataReader.IsClosed) _IDataReader.Close();
            return res;
        }

        /// <summary>
        /// IDataReader 转换 实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDictionaryByIDataReader(this IDataReader _IDataReader)
        {
            if (_IDataReader.IsClosed) throw new HzySqlException("IDataReader 对象连接已关闭！");
            var dic = new Dictionary<string, object>();
            var count = 0;
            while (_IDataReader.Read())
            {
                if (count > 0) break;
                for (int i = 0; i < _IDataReader.FieldCount; i++)
                {
                    if (_IDataReader.IsDBNull(i))
                        dic.Add(_IDataReader.GetName(i), null);
                    else
                        dic.Add(_IDataReader.GetName(i), _IDataReader.GetValue(i));
                }
                count++;
            }
            if (_IDataReader != null && !_IDataReader.IsClosed) _IDataReader.Close();
            return dic;
        }

        /// <summary>
        /// IDataReader 转换为 DataTable
        /// </summary>
        /// <param name="_IDataReader"></param>
        /// <returns></returns>
        public static DataTable ToDataTable(this IDataReader _IDataReader)
        {
            DataTable dt = new DataTable();
            dt.Load(_IDataReader);
            if (_IDataReader != null && !_IDataReader.IsClosed) _IDataReader.Close();
            return dt;
        }


        #endregion

        #region DataRow 转换 

        /// <summary>
        /// DataRow 转换 实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static T ToEntity<T>(this DataRow dr) where T : class, new()
        {
            var _Entity = Utility.CreateInstance<T>();
            var list = _Entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            if (list.Length == 0) throw new HzySqlException("找不到任何 公共属性！");

            foreach (var item in list)
            {
                string AttrName = item.Name;
                if (!dr.Table.Columns.Contains(AttrName)) continue;
                if (dr[AttrName] == DBNull.Value) continue;
                item.SetValue(_Entity, dr[AttrName], null);
            }
            return _Entity;
        }

        #endregion

        #region DataTable 转换

        /// <summary>
        /// 将 datatable 转换为 list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataTable table) where T : class, new()
        {
            var list = new List<T>();
            foreach (DataRow dr in table.Rows)
            {
                var _Entity = Utility.CreateInstance<T>();
                var propertyInfo = _Entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
                foreach (var item in propertyInfo)
                {
                    string AttrName = item.Name;
                    if (!dr.Table.Columns.Contains(AttrName)) continue;
                    if (dr[AttrName] == DBNull.Value) continue;
                    item.SetValue(_Entity, dr[AttrName], null);
                }
                list.Add(_Entity);
            }
            return list;
        }

        /// <summary>
        /// datatable 转换为 List<Dictionary<string,object>>
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> ToList(this DataTable table, string DateTimeStringFormat = "yyyy-MM-dd HH:mm", Action<Type, object> Success = null)
        {
            var list = new List<Dictionary<string, object>>();
            foreach (DataRow dr in table.Rows)
            {
                var dic = new Dictionary<string, object>();
                foreach (DataColumn dc in table.Columns)
                {
                    object value = null;
                    if (dr[dc.ColumnName] != DBNull.Value)
                        value = (dc.DataType == typeof(DateTime)) ? Convert.ToDateTime(dr[dc.ColumnName]).ToString(DateTimeStringFormat) : dr[dc.ColumnName];
                    Success?.Invoke(dc.DataType, value);
                    dic.Add(dc.ColumnName, value);
                }
                list.Add(dic);
            }
            return list;
        }

        #endregion

        /// <summary>
        /// 将匿名对象转换为字典
        /// </summary>
        /// <param name="Attribute"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDic<T>(this T Attribute) where T : class, new()
        {
            var dic = new Dictionary<string, object>();
            Type _type = Attribute.GetType();
            var fields = _type.GetProperties().ToList();
            foreach (var item in fields) dic.Add(item.Name, item.GetValue(Attribute));
            return dic;
        }

        /// <summary>
        /// 将 T 实体中数据 转到 M 实体中去
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="M"></typeparam>
        /// <param name="Entity"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public static M ToNewEntity<T, M>(this T Entity, M Model) where T : class, new()
        {
            //old
            var oldList = Entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var newList = Model.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var oldM in oldList)
            {
                foreach (var newM in newList)
                {
                    if (oldM.Name != newM.Name) continue;
                    if (oldM.PropertyType == typeof(bool) & (newM.PropertyType == typeof(int) | newM.PropertyType == typeof(int?)))
                    {
                        var _Value = (bool)oldM.GetValue(Entity);
                        newM.SetValue(Model, _Value ? 1 : 0);
                    }
                    else if (newM.PropertyType == typeof(bool) & (oldM.PropertyType == typeof(int) | oldM.PropertyType == typeof(int?)))
                    {
                        var _Value = (int?)oldM.GetValue(Entity);
                        newM.SetValue(Model, _Value == null ? false : (_Value == 1 ? true : false));
                    }
                    else
                    {
                        newM.SetValue(Model, oldM.GetValue(Entity));
                    }
                }
            }

            return Model;
        }

        /// <summary>
        /// 将 T 集合数据 copy 到 M 集合中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="M"></typeparam>
        /// <param name="EntityList"></param>
        /// <param name="ModelList"></param>
        /// <returns></returns>
        public static List<M> ToNewList<T, M>(this List<T> EntityList, List<M> ModelList)
            where M : class, new()
            where T : class, new()
        {
            foreach (var item in EntityList)
            {
                var _M = new M();
                item.ToNewEntity(_M);
                ModelList.Add(_M);
            }
            return ModelList;
        }










    }
}
