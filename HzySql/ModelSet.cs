﻿using System;
using System.Collections.Generic;

namespace HzySql
{
    using System.Linq;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using HzySql.Attributes;

    public static class ModelSet
    {
        private static List<Models.FieldInfo> Infos { get; set; } = new List<Models.FieldInfo>();

        /// <summary>
        /// 获取或者换成 字段信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="call">拦截正要存储得字段信息</param>
        /// <returns></returns>
        public static IEnumerable<Models.FieldInfo> GetOrCacheFieldInfo<T>()
            => ModelSet.GetOrCacheFieldInfo(typeof(T));

        /// <summary>
        /// 获取或者换成 字段信息
        /// </summary>
        /// <param name="type"></param>
        /// <param name="call">拦截正要存储得字段信息</param>
        /// <returns></returns>
        public static IEnumerable<Models.FieldInfo> GetOrCacheFieldInfo(Type type)
        {
            var ModelName = type.Name;

            var _Infos = ModelSet.Infos.Where(w => w.ModelName == ModelName);

            if (_Infos?.Count() != 0) return _Infos;

            var TableName = ModelSet.GetTableName(type);

            _Infos = ModelSet.Analysis(type, ModelName, TableName);

            if (_Infos == null) throw new ArgumentNullException();

            ModelSet.Infos.AddRange(_Infos);

            return _Infos;
        }

        /// <summary>
        /// 获取字段信息
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public static IEnumerable<Models.FieldInfo> GetFieldInfos(Func<Models.FieldInfo, bool> func) => Infos.Where(func);

        /// <summary>
        /// 获取所有字段信息
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Models.FieldInfo> GetAll() => Infos;

        /// <summary>
        /// 根据类型获取表名称
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string GetTableName(Type type)
        {
            var _TableAttribute = (TableAttribute)Attribute.GetCustomAttributes(type, true).Where(w => w is TableAttribute).FirstOrDefault();
            if (_TableAttribute == null) return string.Empty;
            return _TableAttribute.Name;
        }

        /// <summary>
        /// 根据类型 模型名称 表名称 解析字段
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ModelName"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        private static List<Models.FieldInfo> Analysis(Type type, string ModelName, string TableName)
        {
            var _PropertyInfos = Utility.GetPropertyInfos(type);
            var _Infos = new List<Models.FieldInfo>();

            for (int i = 0; i < _PropertyInfos.Length; i++)
            {
                var item = _PropertyInfos[i];
                var _TableInfo = new Models.FieldInfo();
                _TableInfo.ModelName = ModelName;
                _TableInfo.TableName = string.IsNullOrWhiteSpace(TableName) ? ModelName : TableName;
                _TableInfo.FieldName = item.Name;
                _TableInfo.FieldType = item.PropertyType;

                //ColumnAttribute
                var _ColumnAttribute = (ColumnAttribute)Attribute.GetCustomAttributes(item, true).Where(w => w is ColumnAttribute).FirstOrDefault();
                if (_ColumnAttribute == null)
                    _TableInfo.ColumnName = item.Name;
                else
                    _TableInfo.ColumnName = _ColumnAttribute.Name;

                //KeyAttribute
                var _KeyAttribute = (KeyAttribute)Attribute.GetCustomAttributes(item, true).Where(w => w is KeyAttribute).FirstOrDefault();
                _TableInfo.IsKey = _KeyAttribute != null;

                //DatabaseGeneratedAttribute
                var _DatabaseGeneratedAttribute = (DatabaseGeneratedAttribute)Attribute.GetCustomAttributes(item, true).Where(w => w is DatabaseGeneratedAttribute).FirstOrDefault();
                if (_DatabaseGeneratedAttribute == null)
                    _TableInfo.DatabaseGeneratedOption = DatabaseGeneratedOption.None;
                else
                    _TableInfo.DatabaseGeneratedOption = _DatabaseGeneratedAttribute.DatabaseGeneratedOption;

                //NotMappedAttribute
                var _NotMappedAttribute = (NotMappedAttribute)Attribute.GetCustomAttributes(item, true).Where(w => w is NotMappedAttribute).FirstOrDefault();
                _TableInfo.NotMapped = _NotMappedAttribute != null;

                //判断 是否有 HzySqlRemarkAttrbibute 特性
                var _HzySqlRemarkAttrbibute = (HzySqlRemarkAttribute)Attribute.GetCustomAttributes(item, true).Where(w => w is HzySqlRemarkAttribute).FirstOrDefault();
                if (_HzySqlRemarkAttrbibute != null) _TableInfo.Remarks = _HzySqlRemarkAttrbibute.Remark;

                HzySqlExtend.CacheFieldInfoCall?.Invoke(item, _TableInfo, type);

                _Infos.Add(_TableInfo);
            }

            return _Infos;
        }

    }
}
