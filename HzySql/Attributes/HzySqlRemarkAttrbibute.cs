﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class HzySqlRemarkAttribute : Attribute
    {
        public string Remark { get; }

        public HzySqlRemarkAttribute(string remark)
        {
            this.Remark = remark;
        }

    }
}
