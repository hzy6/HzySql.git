﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class HzySqlForeignKeyAttribute : Attribute
    {

        public Type TableType { get; }

        public string FiledName { get; }

        public HzySqlForeignKeyAttribute(Type tableType, string filedName)
        {
            this.TableType = tableType;
            this.FiledName = filedName;
        }


    }


}
