﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Models
{
    public class CaseAchieve
    {
        public CaseAchieve When<T>(Expression<Func<bool>> field, T then) => this;

        public CaseAchieve Else<T>(T @else) => this;

        public CaseAchieve End(string Name = null) => this;

    }

}
