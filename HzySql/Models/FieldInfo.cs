﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Models
{
    //using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// https://www.cnblogs.com/caofangsheng/p/10645958.html
    /// </summary>
    public class FieldInfo
    {
        /// <summary>
        /// 实体模型名称
        /// </summary>
        public string ModelName { get; set; }

        /// <summary>
        /// 数据库表名称
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 属性名称
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 数据库映射名称
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// 字段类型
        /// </summary>
        public Type FieldType { get; set; }

        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsKey { get; set; } = false;

        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /// <summary>
        /// 配置列自动生成，可以有三个选项：identity【自增】, computed【计算，添加修改时不队列写入操作】 or none【无】
        /// </summary>
        public DatabaseGeneratedOption DatabaseGeneratedOption { get; set; }

        /// <summary>
        /// 自增
        /// </summary>
        public bool IsIdentity => this.DatabaseGeneratedOption == DatabaseGeneratedOption.Identity;

        /// <summary>
        /// Computed 计算，添加修改时不队列写入操作
        /// </summary>
        public bool IsComputed => this.DatabaseGeneratedOption == DatabaseGeneratedOption.Computed;

        /// <summary>
        /// None
        /// </summary>
        public bool IsDatabaseGeneratedOption_None => this.DatabaseGeneratedOption == DatabaseGeneratedOption.None;

        //NotMapped
        /// <summary>
        /// 忽略列或者表 可以应用于实体或者实体的属性上，应用于实体上，就不会生成表，应用于属性上，就不会生成列
        /// </summary>
        public bool NotMapped { get; set; } = false;

        /// <summary>
        /// 备注 描述
        /// </summary>
        public string Remarks { get; set; }


    }
}
