﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Models
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DataBaseType
    {
        SqlServer,
        MySql,
    }
}
