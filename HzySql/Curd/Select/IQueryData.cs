﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Curd.Select
{
    using HzySql.Interface;
    using HzySql.Models;
    using System.Data;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IQueryData
    {
        IQueryData ToSqlContext(out ISqlContext iSqlContext);
        ISqlContext ToSqlContext();
        string ToSql();
        IQueryData ToSql(out string sqlCode);
        ISqlContext Union(ISqlContext Join);
        ISqlContext UnionAll(ISqlContext Join);

        #region 同步
        int Count();
        TR First<TR>();
        List<TR> ToList<TR>();
        DataTable ToTable();
        Dictionary<string, object> ToDictionary();
        List<Dictionary<string, object>> ToDictionaryList();
        #endregion

        #region 异步
        Task<int> CountAsync();
        Task<TR> FirstAsync<TR>();
        Task<List<TR>> ToListAsync<TR>();
        Task<DataTable> ToTableAsync();
        Task<Dictionary<string, object>> ToDictionaryAsync();
        Task<List<Dictionary<string, object>>> ToDictionaryListAsync();
        #endregion



    }

    public interface IQueryData<T> : IQueryData
    {
        new IQueryData<T> ToSqlContext(out ISqlContext iSqlContext);
        new IQueryData<T> ToSql(out string sqlCode);
        IQueryData<T> Top(int Top);
        IQueryData<T> Distinct();
        #region 分页
        IQueryData<T> TakePage(int Page, int Rows);
        IQueryData<T> TakePage(int Page, int Rows, out int Count);
        #endregion

        #region 同步
        T First();
        List<T> ToList();
        TR Max<TR>(Expression<Func<HzyTuple<T>, TR>> Exp);
        TR Min<TR>(Expression<Func<HzyTuple<T>, TR>> Exp);
        TR Sum<TR>(Expression<Func<HzyTuple<T>, TR>> Exp);
        #endregion

        #region 异步
        Task<T> FirstAsync();
        Task<List<T>> ToListAsync();
        Task<TR> MaxAsync<TR>(Expression<Func<HzyTuple<T>, TR>> Exp);
        Task<TR> MinAsync<TR>(Expression<Func<HzyTuple<T>, TR>> Exp);
        Task<TR> SumAsync<TR>(Expression<Func<HzyTuple<T>, TR>> Exp);
        #endregion



    }

}
