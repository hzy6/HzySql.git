﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Curd
{
    using HzySql.Interface;
    using HzySql.Models;
    using System.Threading.Tasks;

    public interface IUpdate<T> where T : class, new()
    {
        IUpdate<T> NotMapped<TCol>(Expression<Func<T, TCol>> NotMappedColumns, bool IF = true);
        IUpdate<T> Where(Expression<Func<HzyTuple<T>, bool>> Where, bool IF = true);
        string ToSql();
        IUpdate<T> ToSql(out string sqlCode);
        IUpdate<T> ToSqlContext(out ISqlContext iSqlContext);
        ISqlContext ToSqlContext();
        void ToSqlContext(List<ISqlContext> iSqlContexts);
        int Save();
        Task<int> SaveAsync();
    }
}
