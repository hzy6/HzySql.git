﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Curd
{
    using HzySql.Interface;
    using HzySql.Models;
    using System.Threading.Tasks;

    public interface IInsert<T> where T : class, new()
    {
        IInsert<T> NotMapped<TCol>(Expression<Func<T, TCol>> NotMappedColumns, bool IF);
        IInsert<T> GetKeyInfo(out FieldInfo KeyInfo, out object KeyValue);
        string ToSql();
        IInsert<T> ToSql(out string sqlCode);
        IInsert<T> ToSqlContext(out ISqlContext iSqlContext);
        ISqlContext ToSqlContext();
        void ToSqlContext(List<ISqlContext> iSqlContexts);
        TR Save<TR>();
        Task<TR> SaveAsync<TR>();
        int Save();
        Task<int> SaveAsync();

    }
}
