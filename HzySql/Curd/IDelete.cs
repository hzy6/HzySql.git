﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Curd
{
    using HzySql.Interface;
    using HzySql.Models;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IDelete<T> where T : class, new()
    {
        IDelete<T> Where(Expression<Func<HzyTuple<T>, bool>> Where, bool IF = true);
        string ToSql();
        IDelete<T> ToSql(out string sqlCode);
        IDelete<T> ToSqlContext(out ISqlContext iSqlContext);
        ISqlContext ToSqlContext();
        void ToSqlContext(List<ISqlContext> iSqlContexts);
        int Save();
        Task<int> SaveAsync();
    }
}
