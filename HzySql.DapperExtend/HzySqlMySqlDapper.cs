﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.DapperExtend
{
    using MySql.Data.MySqlClient;
    using HzySql.Core.MySql;
    using HzySql.Dapper;

    /// <summary>
    /// MySql Ado 实现
    /// </summary>
    public class HzySqlMySqlDapper : HzySqlMySql
    {
        public HzySqlMySqlDapper(string dbConnectionString) : base()
        {
            this.AdoProvider = new HzySqlAdoProvider(dbConnectionString, () => new MySqlConnection(dbConnectionString));
        }


    }
}
