﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Dapper
{
    using System.Data;
    using System.Threading.Tasks;
    using global::Dapper;
    using HzySql.Interface;
    using HzySql.Models;

    public class HzySqlAdoProvider : IHzySqlAdoProvider
    {
        public virtual Func<IDbConnection> IDbConnectionCall { get; set; }
        public virtual string DbConnectionString { get; set; }
        public virtual IDbConnection DbConnection { get; set; }
        public virtual IDbTransaction DbTransaction { get; set; }
        public virtual bool CommitState { get; set; } = false;
        public virtual int? CommandTimeout { get; set; } = null;

        public HzySqlAdoProvider(string dbConnectionString, Func<IDbConnection> iDbConnectionCall)
        {
            this.DbConnectionString = dbConnectionString;
            IDbConnectionCall = iDbConnectionCall;
        }

        protected virtual IDbConnection GetIDbConnection()
        {
            if (this.CommitState)
            {
                if (this.DbConnection == null) throw new HzySqlException("连接对象不能为NULL！");

                return this.DbConnection;
            }

            if (this.DbConnection == null)
                this.DbConnection = IDbConnectionCall.Invoke();
            if (this.DbConnection.State == ConnectionState.Closed)
                this.DbConnection.Open();
            return this.DbConnection;
        }

        #region 同步方法

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual object ExecuteScalar(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().ExecuteScalar(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual T ExecuteScalar<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().ExecuteScalar<T>(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 返回受影响行数
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual int Execute(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().Execute(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 单行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual T QueryFirstOrDefault<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().QueryFirstOrDefault<T>(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 多行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> Query<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().Query<T>(sql, par.GetDynamicParameters(), dbTransaction, true, this.CommandTimeout);

        /// <summary>
        /// 返回一个 datatable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual DataTable QueryTable(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().QueryDataTable(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 返回 IDataReader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual IDataReader ExecuteReader(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().ExecuteReader(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        #endregion

        #region 异步方法

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual Task<object> ExecuteScalarAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().ExecuteScalarAsync(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 单行单列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual Task<T> ExecuteScalarAsync<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().ExecuteScalarAsync<T>(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 返回受影响行数
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual Task<int> ExecuteAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().ExecuteAsync(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 单行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual Task<T> QueryFirstOrDefaultAsync<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().QueryFirstOrDefaultAsync<T>(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 多行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual Task<IEnumerable<T>> QueryAsync<T>(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().QueryAsync<T>(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 返回一个 datatable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual Task<DataTable> QueryTableAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().QueryDataTableAsync(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        /// <summary>
        /// 返回 IDataReader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="par"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public virtual Task<IDataReader> ExecuteReaderAsync(string sql, List<DataParameter> par, IDbTransaction dbTransaction = null)
            => this.GetIDbConnection().ExecuteReaderAsync(sql, par.GetDynamicParameters(), dbTransaction, this.CommandTimeout);

        #endregion

        #region 事务

        /// <summary>
        /// 开启一个事务
        /// </summary>
        /// <returns></returns>
        public virtual IDbTransaction BeginTransaction()
        {
            this.CommitState = true;

            this.DbConnection = IDbConnectionCall.Invoke();

            if (this.DbConnection.State == ConnectionState.Closed)
                this.DbConnection.Open();

            this.DbTransaction = this.GetIDbConnection().BeginTransaction();

            return this.DbTransaction;
        }

        /// <summary>
        /// 提交完成事务
        /// </summary>
        public virtual void Commit()
        {
            if (!this.CommitState) throw new HzySqlException("事务暂未开启 , 请先 调用 BeginTransaction 开启事务!");

            try
            {
                this.DbTransaction.Commit();
            }
            catch (Exception ex)
            {
                HzySqlExtend.ExceptionCall?.Invoke(ex);
                this.Rollback();
                throw;
            }
            finally
            {
                this.Rollback();
            }
        }

        /// <summary>
        /// 回滚
        /// </summary>
        public virtual void Rollback()
        {
            if (!this.CommitState) return;

            this.CommitState = false;

            if (this.DbTransaction != null && this.DbTransaction.Connection != null)
            {
                this.DbTransaction.Rollback();
                this.DbTransaction.Dispose();
            }

            if (this.DbConnection != null)
            {
                this.DbConnection.Close();
                this.DbTransaction.Dispose();
            }

            this.DbConnection = null;
            this.DbTransaction = null;
        }

        #endregion


    }
}
