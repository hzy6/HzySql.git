﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Dapper
{
    using System.Data;
    using System.Threading.Tasks;
    using global::Dapper;
    using HzySql.Models;
    
    /// <summary>
    /// hsql dapper 扩展
    /// </summary>
    public static class HzySqlDapperExtend
    {
        public static DataTable QueryDataTable(this IDbConnection dbConnection, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) => dbConnection.ExecuteReader(sql, param, transaction, commandTimeout, commandType).ToDataTable();

        public static async Task<DataTable> QueryDataTableAsync(this IDbConnection dbConnection, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) => (await dbConnection.ExecuteReaderAsync(sql, param, transaction, commandTimeout, commandType)).ToDataTable();

        public static DynamicParameters GetDynamicParameters(this List<DataParameter> Parameter)
        {
            if (Parameter == null) return null;

            if (Parameter.Count == 0) return null;

            var _DynamicParameters = new DynamicParameters();
            foreach (var item in Parameter)
            {
                _DynamicParameters.Add(item.ParameterName, item.Value);
            }
            return _DynamicParameters;
        }



    }
}
