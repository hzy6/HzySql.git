﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Dapper.SqlServer
{
    using System.Data.SqlClient;
    using HzySql.Core.SqlServer;
    
    /// <summary>
    /// SqlServer Ado 实现
    /// </summary>
    public class HzySqlSqlServerDapper : HzySqlSqlServer
    {
        public HzySqlSqlServerDapper(string dbConnectionString) : base()
        {
            this.AdoProvider = new HzySqlAdoProvider(dbConnectionString, () => new SqlConnection(dbConnectionString));
        }

        public HzySqlSqlServerDapper(string dbConnectionString, PagingMode pagingMode) : base(pagingMode)
        {
            this.AdoProvider = new HzySqlAdoProvider(dbConnectionString, () => new SqlConnection(dbConnectionString));
        }


    }
}
