﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Dapper.MySql
{
    using global::MySql.Data.MySqlClient;
    using HzySql.Core.MySql;
    using HzySql.Dapper;

    public class HzySqlMySqlDapper : HzySqlMySql
    {
        public HzySqlMySqlDapper(string dbConnectionString) : base()
        {
            this.AdoProvider = new HzySqlAdoProvider(dbConnectionString, () => new MySqlConnection(dbConnectionString));
        }






    }
}
