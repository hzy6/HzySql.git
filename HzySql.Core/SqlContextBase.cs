﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core
{
    using System.Data;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using HzySql.Core.Parser;
    using HzySql.Interface;
    using HzySql.Models;

    /// <summary>
    /// Sql 上下文
    /// </summary>
    public class SqlContextBase : Abstract.AbsSqlContext
    {
        public SqlContextBase() { }

        public SqlContextBase(IHzySqlAdoProvider adoProvider)
        {
            this.AdoProvider = adoProvider;
        }

        public override IHzySqlAdoProvider AdoProvider { get; set; }
        public override bool IsAlias { get; set; } = true;
        public override List<DataParameter> Parameter { get; set; } = new List<DataParameter>();
        public override Dictionary<string, string> Alias { get; set; } = new Dictionary<string, string>();
        public override DataBaseType DataBaseType { get; set; } = DataBaseType.SqlServer;
        public override string Symbol(string Name) => throw new NotImplementedException();
        public override string ParametricSymbols { get; } = "@";
        public override Func<ISqlContext> NewContextCall { get; set; }

        #region Insert、Update、Delete、Select
        /// <summary>
        /// 插入语句
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="Cols"></param>
        /// <param name="Values"></param>
        /// <returns></returns>
        public override void CreateInsertSql(string TableName, List<string> Cols, List<string> Values)
        {
            this.Code.Clear();
            this.Code.Append($"INSERT INTO {this.Symbol(TableName)} ({string.Join(",", Cols)}) VALUES({string.Join(",", Values)});");
        }

        /// <summary>
        /// 更新语句
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="Sets"></param>
        /// <returns></returns>
        public override void CreateUpdateSql(string TableName, List<string> Sets)
        {
            this.Code.Clear();
            this.Code.Append($"UPDATE {this.Symbol(TableName)} SET {string.Join(",", Sets)} WHERE 1=1{this.Where};");
        }

        /// <summary>
        /// 删除语句
        /// </summary>
        /// <param name="TableName"></param>
        public override void CreateDeleteSql(string TableName)
        {
            this.Code.Clear();
            this.Code.Append($"DELETE FROM {this.Symbol(TableName)} WHERE 1=1{this.Where};");
        }

        /// <summary>
        /// 转换为最终Sql
        /// </summary>
        public override ISqlContext ToSql()
        {
            //组装Sql
            this.Code.Clear();
            this.Code.Append(this.ToSql(
                 this.Cols,
                 this.From,
                 this.Join,
                 this.Where,
                 this.GroupBy,
                 this.Having,
                 this.OrderBy,
                 this.TakePage
                 ));
            return this;
        }

        public override StringBuilder ToSql(StringBuilder Cols, StringBuilder From, StringBuilder Join, StringBuilder Where, StringBuilder GroupBy, StringBuilder Having, StringBuilder OrderBy, StringBuilder TakePage)
            => throw new NotImplementedException();

        /// <summary>
        /// 转换为最终Sql 字符串
        /// </summary>
        /// <returns></returns>
        public override string ToSqlString()
        {
            var sql = this.Code.ToString();

            foreach (var item in this.Parameter)
            {
                sql = sql.Replace(item.ParameterName, item.Value == null ? "NULL" : $"'{item.Value.ToString()}'");
            }

            return sql;
        }

        public override void CreateTakePageSql(int Page, int Rows) => throw new NotImplementedException();

        /// <summary>
        /// From
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public override void CreateFromSql<T>()
        {
            var _TableInfo = ModelSet.GetOrCacheFieldInfo<T>();
            var TableName = this.Symbol(_TableInfo.FirstOrDefault().TableName);

            var _Alias = this.Symbol(nameof(HzyTuple<T>.t1));

            this.Alias[_Alias] = TableName;
            this.Cols.Append("*");
            this.From.Append($"{TableName} AS {_Alias} <#LockWith#>");
        }

        /// <summary>
        /// Select
        /// </summary>
        /// <param name="Lambda"></param>
        public override void CreateSelectSql(LambdaExpression Lambda)
        {
            var _SelectParser = new SelectParser(this);
            _SelectParser.Visit(Lambda);
        }

        /// <summary>
        /// Where
        /// </summary>
        /// <param name="Lambda"></param>
        public override void CreateWhereSql(LambdaExpression Lambda, string StartSymbol = "AND")
        {
            this.Where.Append(" " + StartSymbol);
            var _WhereParser = new WhereParser(this);
            _WhereParser.Visit(Lambda);
        }

        /// <summary>
        /// Group By
        /// </summary>
        /// <param name="Lambda"></param>
        public override void CreateGroupBySql(LambdaExpression Lambda)
        {
            var _GroupByParser = new GroupByParser(this);
            _GroupByParser.Visit(Lambda);
        }

        /// <summary>
        /// Join
        /// </summary>
        /// <param name="Lambda"></param>
        public override void CreateJoinSql(LambdaExpression Lambda, JoinType _JoinType)
        {
            var _JoinParser = new JoinParser(this);
            _JoinParser.Create(Lambda, _JoinType);
        }

        /// <summary>
        /// Order By
        /// </summary>
        /// <param name="Lambda"></param>
        public override void CreateOrderBySql(LambdaExpression Lambda)
        {
            var _OrderByParser = new OrderByParser(this);
            _OrderByParser.Visit(Lambda);
        }

        /// <summary>
        /// Order By Desc
        /// </summary>
        /// <param name="Lambda"></param>
        public override void CreateOrderByDescSql(LambdaExpression Lambda)
        {
            var _OrderByParser = new OrderByParser(this);
            _OrderByParser.CreateDESC(Lambda);
        }

        /// <summary>
        /// Having
        /// </summary>
        /// <param name="Lambda"></param>
        public override void CreateHavingSql(LambdaExpression Lambda)
        {
            var NewContext = this.NewContextCall.Invoke();
            NewContext.Parameter = this.Parameter;
            NewContext.Alias = this.Alias;
            NewContext.IsAlias = this.IsAlias;
            NewContext.CreateWhereSql(Lambda);

            this.Having.Append(NewContext.Where);
            this.Parameter = NewContext.Parameter;
        }
        /// <summary>
        /// 去重
        /// </summary>
        public override void CreateDistinctSql()
        {
            if (!this.Cols.ToString().ToLower().Contains("DISTINCT".ToLower())) this.Cols.Insert(0, "DISTINCT ");
        }

        public override void CreateTopSql(int Top) => throw new NotImplementedException();

        /// <summary>
        /// UNION
        /// </summary>
        /// <param name="Union"></param>
        /// <param name="Symbol"></param>
        /// <returns></returns>
        public override ISqlContext CreateUnion(ISqlContext Union, string Symbol = "Sub")
        {
            this.ToSql();
            foreach (var item in Union.Parameter)
            {
                var Length = this.Parameter.Count;
                var Name = $"{this.ParametricSymbols}{Symbol}P{Length}";
                this.Parameter.Add(new DataParameter(Name, item.Value));
                Union.Code.Replace(item.ParameterName, Name);
            }
            return Union;
        }

        public override void CreateMaxSql(LambdaExpression Lambda) => new Max_Min_Sum_Parser(this, "MAX").Visit(Lambda);
        public override void CreateMinSql(LambdaExpression Lambda) => new Max_Min_Sum_Parser(this, "MIN").Visit(Lambda);
        public override void CreateSumSql(LambdaExpression Lambda) => new Max_Min_Sum_Parser(this, "SUM").Visit(Lambda);
        public override StringBuilder CreateCountSql()
        {
            this.Code.Clear();
            var SqlString = this.ToSql(
                 this.Cols,
                 this.From,
                 this.Join,
                 this.Where,
                 this.GroupBy,
                 this.Having,
                 new StringBuilder(),
                 new StringBuilder()
                );
            this.Code.Append($"SELECT COUNT(1) FROM ({SqlString}) DATACOUNT");
            return this.Code;
        }

        public override void CreateLockWith(bool isLock) => throw new NotImplementedException();
        #endregion

        #region 数据转换

        #region 同步
        public override int ToCount()
        {
            this.CheckAdoProviderNull();
            this.CreateCountSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalar<int>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override TR ToFirst<TR>()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.QueryFirstOrDefault<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override IEnumerable<TR> ToList<TR>()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.Query<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override DataTable ToTable()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.QueryTable(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override TR ToMax<TR>(LambdaExpression Lambda)
        {
            this.CheckAdoProviderNull();
            this.CreateMaxSql(Lambda);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalar<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override TR ToMin<TR>(LambdaExpression Lambda)
        {
            this.CheckAdoProviderNull();
            this.CreateMinSql(Lambda);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalar<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override TR ToSum<TR>(LambdaExpression Lambda)
        {
            this.CheckAdoProviderNull();
            this.CreateSumSql(Lambda);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalar<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Dictionary<string, object> ToDictionary()
        {
            this.CheckAdoProviderNull();
            this.CreateTopSql(1);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteReader(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction).ToDictionaryByIDataReader();
        }
        public override List<Dictionary<string, object>> ToDictionaryList()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteReader(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction).ToDictionaryListByIDataReader();
        }
        #endregion

        #region 异步
        public override Task<int> ToCountAsync()
        {
            this.CheckAdoProviderNull();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalarAsync<int>(this.CreateCountSql().ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<TR> ToFirstAsync<TR>()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.QueryFirstOrDefaultAsync<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<IEnumerable<TR>> ToListAsync<TR>()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.QueryAsync<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<DataTable> ToTableAsync()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.QueryTableAsync(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<TR> ToMaxAsync<TR>(LambdaExpression Lambda)
        {
            this.CheckAdoProviderNull();
            this.CreateMaxSql(Lambda);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalarAsync<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<TR> ToMinAsync<TR>(LambdaExpression Lambda)
        {
            this.CheckAdoProviderNull();
            this.CreateMinSql(Lambda);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalarAsync<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<TR> ToSumAsync<TR>(LambdaExpression Lambda)
        {
            this.CheckAdoProviderNull();
            this.CreateSumSql(Lambda);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalarAsync<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override async Task<Dictionary<string, object>> ToDictionaryAsync()
        {
            this.CheckAdoProviderNull();
            this.CreateTopSql(1);
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return (await this.AdoProvider.ExecuteReaderAsync(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction)).ToDictionaryByIDataReader();
        }
        public override async Task<List<Dictionary<string, object>>> ToDictionaryListAsync()
        {
            this.CheckAdoProviderNull();
            this.ToSql();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return (await this.AdoProvider.ExecuteReaderAsync(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction)).ToDictionaryListByIDataReader();
        }
        #endregion

        #region Insert Or Update Or Delete
        public override TR InsertToSave<TR>() => throw new NotImplementedException();
        public override Task<TR> InsertToSaveAsync<TR>() => throw new NotImplementedException();
        public override int InsertOrUpdateOrDeleteToSave()
        {
            this.CheckAdoProviderNull();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.Execute(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<int> InsertOrUpdateOrDeleteToSaveAsync()
        {
            this.CheckAdoProviderNull();
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteAsync(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        #endregion

        public override void CheckAdoProviderNull()
        {
            if (this.AdoProvider == null) throw new HzySqlException(" AdoProvider 未被实现，无法获取数据！");
        }

        #endregion

        #region 获取用到的表集合

        public override List<string> GetTableNames() => throw new NotImplementedException();

        #endregion

    }
}
