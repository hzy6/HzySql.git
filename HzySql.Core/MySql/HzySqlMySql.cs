﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.MySql
{
    using HzySql.Interface;
    using HzySql.Core;

    /// <summary>
    /// MySql 上下文
    /// </summary>
    public class HzySqlMySql : HzySqlBase, IHzySql
    {
        public override ISqlContext Context => new SqlContextMySql(AdoProvider);

    }
}
