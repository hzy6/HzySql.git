﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.MySql
{
    using System.Linq;
    using System.Threading.Tasks;
    using HzySql;
    using HzySql.Interface;
    using HzySql.Models;

    public class SqlContextMySql : SqlContextBase, ISqlContext
    {
        public SqlContextMySql(IHzySqlAdoProvider adoProvider)
            : base(adoProvider)
        {
            this.DataBaseType = DataBaseType.MySql;
            this.NewContextCall = () => new SqlContextMySql(adoProvider);
        }

        /// <summary>
        /// 特殊符号
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public override string Symbol(string Name) => $"`{Name}`";

        /// <summary>
        /// 参数化特殊符号
        /// </summary>
        public override string ParametricSymbols => "?";

        #region Insert、Update、Delete、Select

        /// <summary>
        /// 转换为最终Sql
        /// </summary>
        /// <param name="Cols"></param>
        /// <param name="From"></param>
        /// <param name="Join"></param>
        /// <param name="Where"></param>
        /// <param name="GroupBy"></param>
        /// <param name="Having"></param>
        /// <param name="OrderBy"></param>
        /// <param name="TakePage"></param>
        /// <returns></returns>
        public override StringBuilder ToSql(StringBuilder Cols, StringBuilder From, StringBuilder Join, StringBuilder Where, StringBuilder GroupBy, StringBuilder Having, StringBuilder OrderBy, StringBuilder TakePage)
        {
            var sb = new StringBuilder();
            //分页
            var orderByString = OrderBy.ToString();
            if (TakePage.Length > 0) orderByString += TakePage;
            //组装
            sb.Append($"SELECT {Cols}\r\nFROM {From}");
            if (Join.Length > 0) sb.Append(Join);
            if (Where.Length > 0) sb.Append($"\r\nWHERE 1=1 {Where}");
            if (GroupBy.Length > 0) sb.Append($"\r\nGROUP BY {GroupBy}");
            if (Having.Length > 0) sb.Append($"\r\nHAVING {Having}");
            if (!string.IsNullOrWhiteSpace(orderByString)) sb.Append($"\r\nORDER BY {orderByString}");
            //lock with
            sb.Replace("<#LockWith#>", this.LockWith.ToString());
            return sb;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Select"></param>
        public override void CreateTakePageSql(int Page, int Rows)
        {
            if (this.OrderBy.Length == 0) this.OrderBy.Append("1");
            this.TakePage.Clear().Append($" LIMIT {(Rows * (Page - 1))},{Page} ");
        }

        /// <summary>
        /// Top
        /// </summary>
        /// <param name="Top"></param>
        public override void CreateTopSql(int Top)
        {
            if (this.TakePage.Length == 0) this.OrderBy.Append($" LIMIT {Top}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isLock"></param>
        public override void CreateLockWith(bool isLock)
        {
            if (isLock)
            {
                //for update nowait
                this.LockWith.Append("FOR UPDATE NOWAIT");
            }
        }

        #endregion

        #region 数据转换

        public override TR InsertToSave<TR>()
        {
            this.CheckAdoProviderNull();
            this.Code.Append("SELECT LAST_INSERT_ID();");
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalar<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<TR> InsertToSaveAsync<TR>()
        {
            this.CheckAdoProviderNull();
            this.Code.Append("SELECT LAST_INSERT_ID();");
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalarAsync<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }

        #endregion

        #region 获取用到的表集合

        public override List<string> GetTableNames()
        {
            var names = new List<string>();
            foreach (var item in this.Alias.Values)
            {
                var tabName = item.Replace("`", "");
                if (names.Any(w => w == tabName)) continue;
                names.Add(tabName);
            }
            return names;
        }

        #endregion

    }
}
