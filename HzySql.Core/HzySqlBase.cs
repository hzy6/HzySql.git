﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core
{
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using HzySql.Abstract;
    using HzySql.Curd;
    using HzySql.Curd.Select;
    using HzySql.Interface;
    using HzySql.Models;
    using HzySql.Core.Curd;
    using HzySql.Core.Curd.Select;
    using System.Reflection;

    public class HzySqlBase : AbsHzySql
    {
        public HzySqlBase() { }
        public override IHzySqlAdoProvider AdoProvider { get; set; }
        public override ISqlContext Context { get; set; }

        #region Insert
        public override IInsert<T> Insert<T>(Expression<Func<T>> Entity)
            => new InsertAchieve<T>(this.Context, Entity.Body as MemberInitExpression);
        public override IInsert<T> Insert<T>(T Entity)
            => new InsertAchieve<T>(this.Context, Utility.ModelToMemberInitExpression(Entity));
        #endregion

        #region Update
        public override IUpdate<T> Update<T>(Expression<Func<T>> Entity)
            => new UpdateAchieve<T>(this.Context, Entity.Body as MemberInitExpression);
        public override IUpdate<T> Update<T>(Expression<Func<T, T>> Entity)
            => new UpdateAchieve<T>(this.Context, Entity.Body as MemberInitExpression);
        public override IUpdate<T> Update<T>(T Entity)
            => new UpdateAchieve<T>(this.Context, Utility.ModelToMemberInitExpression(Entity));
        public override IUpdate<T> UpdateById<T>(T Entity)
            => this.Update(Entity).Where(this.GetIdWhereByEntity<T>(Entity));
        public override IUpdate<T> Update<T>(Expression<Func<T>> Entity, Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Update(Entity).Where(Where);
        public override IUpdate<T> Update<T>(Expression<Func<T, T>> Entity, Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Update(Entity).Where(Where);
        public override IUpdate<T> Update<T>(T Entity, Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Update(Entity).Where(Where);
        #endregion

        #region Delete
        public override IDelete<T> Delete<T>()
            => new DeleteAchieve<T>(this.Context);
        public override IDelete<T> DeleteById<T>(object Id)
            => this.Delete<T>().Where(this.GetIdWhereById<T>(Id));
        public override IDelete<T> Delete<T>(Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Delete<T>().Where(Where);
        #endregion

        #region 批量执行 Batch Execute
        private (StringBuilder, List<DataParameter>) MergeSqlCode(List<ISqlContext> iSqlCodeContexts)
        {
            var builder = new StringBuilder();
            var parameterList = new List<DataParameter>();
            for (int i = 0; i < iSqlCodeContexts.Count; i++)
            {
                var item = iSqlCodeContexts[i];
                var sql = item.Code.ToString();
                for (int j = item.Parameter.Count - 1; j >= 0; j--)
                {
                    var par = item.Parameter[j];
                    var newParameterName = $"{item.ParametricSymbols}Batch{i}_{par.ParameterName.Replace(item.ParametricSymbols, "")}";
                    sql = sql.Replace(par.ParameterName, newParameterName);
                    parameterList.Add(new DataParameter(newParameterName, par.Value));
                }
                builder.Append(sql);
            }
            return (builder, parameterList);
        }
        public override int ExecuteBatch(List<ISqlContext> iSqlCodeContexts)
        {
            var sqlCode = this.MergeSqlCode(iSqlCodeContexts);
            return this.AdoProvider.Execute(sqlCode.Item1.ToString(), sqlCode.Item2);
        }
        public override Task<int> ExecuteBatchAsync(List<ISqlContext> iSqlCodeContexts)
        {
            var sqlCode = this.MergeSqlCode(iSqlCodeContexts);
            return this.AdoProvider.ExecuteAsync(sqlCode.Item1.ToString(), sqlCode.Item2);
        }
        #endregion

        #region Select 查询
        public override Expression<Func<HzyTuple<T>, bool>> GetIdWhereByEntity<T>(T Entity)
        {
            var _TableInfo = ModelSet.GetOrCacheFieldInfo<T>();
            var First = _TableInfo.FirstOrDefault();
            var KeyInfo = _TableInfo.FirstOrDefault(w => w.IsKey);
            if (KeyInfo == null) throw new HzySqlException($"实体模型{First.ModelName}没有设置主键");
            var KeyValue = Entity.GetType().GetProperty(KeyInfo.FieldName).GetValue(Entity);
            return Utility.IdWhere<T>(KeyInfo.FieldName, KeyValue, First.TableName);
        }
        public override Expression<Func<HzyTuple<T>, bool>> GetIdWhereById<T>(object Id)
        {
            var _TableInfo = ModelSet.GetOrCacheFieldInfo<T>();
            var First = _TableInfo.FirstOrDefault();
            var KeyInfo = _TableInfo.FirstOrDefault(w => w.IsKey);
            if (KeyInfo == null) throw new HzySqlException($"实体模型{First.ModelName}没有设置主键");
            return Utility.IdWhere<T>(KeyInfo.FieldName, Id, First.TableName);
        }
        public override IQuery<T> Query<T>()
            => new QueryAchieve<T>(this.Context);
        public override IQuery<T> Query<T>(Expression<Func<HzyTuple<T>, bool>> Exp)
            => this.Query<T>().Where(Exp);

        #region 同步
        public override T Find<T>(Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Query<T>(Where).First();
        public override T FindById<T>(object Id)
            => this.Query<T>(this.GetIdWhereById<T>(Id)).First();
        public override List<T> FindList<T>(Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Query<T>(Where).ToList();
        public override List<T> FindList<T>(Expression<Func<HzyTuple<T>, bool>> Where, Expression<Func<HzyTuple<T>, object>> OrderBy)
            => this.Query<T>(Where).OrderBy(OrderBy).ToList();
        #endregion
        #region Async 异步
        public override Task<T> FindAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Query<T>(Where).FirstAsync();
        public override Task<T> FindByIdAsync<T>(object Id)
            => this.Query<T>(this.GetIdWhereById<T>(Id)).FirstAsync();
        public override Task<List<T>> FindListAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where)
            => this.Query<T>(Where).ToListAsync();
        public override Task<List<T>> FindListAsync<T>(Expression<Func<HzyTuple<T>, bool>> Where, Expression<Func<HzyTuple<T>, object>> OrderBy)
            => this.Query<T>(Where).OrderBy(OrderBy).ToListAsync();
        #endregion

        #endregion

        #region Aop 拦截器
        public override void UseAopCacheFieldInfoCall(Action<PropertyInfo, Models.FieldInfo, Type> call)
            => HzySqlExtend.CacheFieldInfoCall = call;
        public override void UseAopInsertBeforeCall(Action call)
            => HzySqlExtend.InsertBeforeCall = call;
        public override void UseAopUpdateBeforeCall(Action call)
            => HzySqlExtend.UpdateBeforeCall = call;
        public override void UseAopExecuteSqlBeforeCall(Action<ISqlContext, string> call)
            => HzySqlExtend.ExecuteSqlBeforeCall = call;
        public override void UseAopExceptionCall(Action<Exception> call)
            => HzySqlExtend.ExceptionCall = call;
        #endregion

    }
}
