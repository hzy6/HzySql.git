﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Core.Curd
{
    using HzySql;
    using HzySql.Curd;
    using HzySql.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using HzySql.Core.Parser;
    using HzySql.Interface;

    /// <summary>
    /// 修改对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UpdateAchieve<T> : InsertOrUpdateOrDelete<T>, IUpdate<T> where T : class, new()
    {
        public UpdateAchieve(ISqlContext Context, T Model) : base(Context)
        {
            this._MemberInitExpression = Utility.ModelToMemberInitExpression(Model);
            HzySqlExtend.UpdateBeforeCall?.Invoke();
        }

        public UpdateAchieve(ISqlContext Context, MemberInitExpression memberInitExpression) : base(Context)
        {
            this._MemberInitExpression = memberInitExpression;
            HzySqlExtend.UpdateBeforeCall?.Invoke();
        }

        /// <summary>
        /// 忽略一列
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="NotMappedColumns">列</param>
        /// <param name="IF">如果为true 则 执行 否则 不执行</param>
        /// <returns></returns>
        public virtual IUpdate<T> NotMapped<F>(Expression<Func<T, F>> NotMappedColumns, bool IF)
        {
            if (IF) this.NotMappedColumns.Add(this.NotMappedColAnalysis(NotMappedColumns));
            return this;
        }

        /// <summary>
        /// Where 条件
        /// </summary>
        /// <param name="Where">条件</param>
        /// <param name="IF">如果为true 则 执行 否则 不执行</param>
        /// <returns></returns>
        public virtual IUpdate<T> Where(Expression<Func<HzyTuple<T>, bool>> Where, bool IF)
        {
            if (Where == null || !IF) return this;
            this.Context.CreateWhereSql(Where);
            return this;
        }

        /// <summary>
        /// 转换 Sql 语句
        /// </summary>
        /// <returns></returns>
        public virtual string ToSql()
        {
            var _Sets = new List<string>();

            //加入忽略字段 两个集合合并
            this.NotMappedColumns = this.NotMappedColumns.Union(this.Fields.Where(w => w.IsComputed | w.IsIdentity | w.NotMapped | w.IsKey).Select(w => w.FieldName)).ToList();

            foreach (MemberAssignment item in this._MemberInitExpression.Bindings)
            {
                var _MemberName = item.Member.Name;
                if (this.NotMappedColumns.Any(w => w == _MemberName)) continue;
                var _ColName = this.Fields.FirstOrDefault(w => w.FieldName == _MemberName).ColumnName;

                if (item.Expression is BinaryExpression)
                {
                    var _UpdateParser = new UpdateParser();
                    _UpdateParser.Visit(item.Expression);
                    _Sets.Add($"{this.Context.Symbol(_ColName)}" + "=" + _UpdateParser._StringBuilder);
                }
                else
                {
                    var _Value = Utility.Eval(item.Expression);
                    var _ValueName = $"{this.Context.ParametricSymbols}P{this.Context.Parameter.Count}";

                    _Sets.Add($"{this.Context.Symbol(_ColName)}={_ValueName}");

                    this.Context.Parameter.Add(new DataParameter(_ValueName, _Value));
                }
            }

            this.Context.CreateUpdateSql(TableName, _Sets);
            return this.Context.ToSqlString();
        }

        public virtual IUpdate<T> ToSql(out string sqlCode)
        {
            sqlCode = this.ToSql();
            return this;
        }

        public virtual IUpdate<T> ToSqlContext(out ISqlContext iSqlContext)
        {
            this.ToSql();
            iSqlContext = this.Context;
            return this;
        }

        public virtual ISqlContext ToSqlContext()
        {
            this.ToSql();
            return this.Context;
        }

        public virtual void ToSqlContext(List<ISqlContext> iSqlContexts) => iSqlContexts.Add(this.ToSqlContext());

        public virtual int Save()
        {
            this.ToSql();
            return this.Context.InsertOrUpdateOrDeleteToSave();
        }

        public virtual Task<int> SaveAsync()
        {
            this.ToSql();
            return this.Context.InsertOrUpdateOrDeleteToSaveAsync();
        }
    }
}
