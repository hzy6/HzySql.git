﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Core.Curd
{
    using HzySql;
    using HzySql.Interface;
    using HzySql.Models;
    using System.Linq;

    public class InsertOrUpdateOrDelete<T> : Base
    {
        protected MemberInitExpression _MemberInitExpression { get; set; }
        protected List<string> NotMappedColumns = new List<string>();
        protected List<FieldInfo> Fields { get; set; }
        protected string TableName { get; set; }

        public InsertOrUpdateOrDelete(ISqlContext Context) : base(Context)
        {
            this.Context.IsAlias = false;
            this.Fields = ModelSet.GetOrCacheFieldInfo<T>().ToList();
            this.TableName = this.Fields.FirstOrDefault().TableName;
        }

        /// <summary>
        /// 忽略列解析
        /// </summary>
        /// <param name="NotMappedColumns"></param>
        /// <returns></returns>
        protected string NotMappedColAnalysis<F>(Expression<Func<T, F>> NotMappedColumns)
        {
            var _Body = NotMappedColumns.Body;

            if (_Body is UnaryExpression)
            {
                var _UnaryExpression = _Body as UnaryExpression;
                var _Operand = _UnaryExpression.Operand;
                if (_Operand is MemberExpression)
                {
                    var _MemberExpression = _Operand as MemberExpression;
                    return _MemberExpression.Member.Name;
                }
            }
            else if (_Body is MemberExpression)
            {
                var _MemberExpression = _Body as MemberExpression;
                return _MemberExpression.Member.Name;
            }

            throw new HzySqlException("NotMapped 语法无法解析!");
        }



    }
}
