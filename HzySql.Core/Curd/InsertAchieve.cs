﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HzySql.Core.Curd
{
    using HzySql;
    using HzySql.Curd;
    using HzySql.Interface;
    using HzySql.Models;
    using System.Threading.Tasks;

    /// <summary>
    /// 插入对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InsertAchieve<T> : InsertOrUpdateOrDelete<T>, IInsert<T>
        where T : class, new()
    {
        private object KeyValue { get; set; }

        public InsertAchieve(ISqlContext Context, T Model) : base(Context)
        {
            this._MemberInitExpression = Utility.ModelToMemberInitExpression(Model);
            HzySqlExtend.InsertBeforeCall?.Invoke();
        }

        public InsertAchieve(ISqlContext Context, MemberInitExpression memberInitExpression) : base(Context)
        {
            this._MemberInitExpression = memberInitExpression;
            HzySqlExtend.InsertBeforeCall?.Invoke();
        }

        /// <summary>
        /// 忽略一列
        /// </summary>
        /// <typeparam name="F"></typeparam>
        /// <param name="NotMappedColumns">列</param>
        /// <param name="IF">如果为true 则 执行 否则 不执行</param>
        /// <returns></returns>
        public virtual IInsert<T> NotMapped<F>(Expression<Func<T, F>> NotMappedColumns, bool IF)
        {
            if (IF) this.NotMappedColumns.Add(this.NotMappedColAnalysis(NotMappedColumns));
            return this;
        }

        /// <summary>
        /// 获取Key
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public virtual IInsert<T> GetKeyInfo(out FieldInfo KeyInfo, out object KeyValue)
        {
            KeyInfo = this.Fields.Find(w => w.IsKey);
            KeyValue = this.KeyValue;
            return this;
        }

        /// <summary>
        /// 转换 Sql 语句
        /// </summary>
        /// <returns></returns>
        public virtual string ToSql()
        {
            if (this.Context.Parameter.Count > 0) this.Context.Parameter = new List<DataParameter>();
            var _Cols = new List<string>();
            var _Values = new List<string>();

            //加入忽略字段 两个集合合并
            this.NotMappedColumns = this.NotMappedColumns.Union(this.Fields.Where(w => w.IsComputed | w.IsIdentity | w.NotMapped).Select(w => w.FieldName)).ToList();

            foreach (MemberAssignment item in this._MemberInitExpression.Bindings)
            {
                var _MemberName = item.Member.Name;
                if (this.NotMappedColumns.Any(w => w == _MemberName)) continue;
                var Field = this.Fields.FirstOrDefault(w => w.FieldName == _MemberName);
                if (Field == null) continue;
                var _ColName = Field.ColumnName;
                var _Value = Utility.Eval(item.Expression);

                //KeyValue
                if (Field.IsKey)
                {
                    this.KeyValue = this.CheckKey(Field.FieldType, _Value);
                    _Value = this.KeyValue;
                }

                var _Col = $"{this.Context.Symbol(_ColName)}";
                var _ValueName = $"{this.Context.ParametricSymbols}P{this.Context.Parameter.Count}";

                _Cols.Add(_Col);
                _Values.Add(_ValueName);

                this.Context.Parameter.Add(new DataParameter(_ValueName, _Value));
            }

            this.Context.CreateInsertSql(TableName, _Cols, _Values);
            return this.Context.ToSqlString();
        }

        public virtual IInsert<T> ToSql(out string sqlCode)
        {
            sqlCode = this.ToSql();
            return this;
        }

        public virtual IInsert<T> ToSqlContext(out ISqlContext iSqlContext)
        {
            this.ToSql();
            iSqlContext = this.Context;
            return this;
        }

        public virtual ISqlContext ToSqlContext()
        {
            this.ToSql();
            return this.Context;
        }

        public virtual void ToSqlContext(List<ISqlContext> iSqlContexts) => iSqlContexts.Add(this.ToSqlContext());

        private object CheckKey(Type type, object Value)
        {
            if (type == typeof(Guid) || type == typeof(Guid?))
            {
                if (Value == null || Guid.Parse(Value.ToString()) == Guid.Empty) Value = Guid.NewGuid();
            }

            return Value;
        }

        public virtual TR Save<TR>()
        {
            try
            {
                this.ToSql();
                if (KeyValue == null)
                {
                    return this.Context.InsertToSave<TR>();
                }
                else
                {
                    this.Save();
                    return (TR)KeyValue;
                }
            }
            catch (Exception)
            {
                return default;
            }
        }

        public virtual async Task<TR> SaveAsync<TR>()
        {
            try
            {
                this.ToSql();
                if (KeyValue == null)
                {
                    return await this.Context.InsertToSaveAsync<TR>();
                }
                else
                {
                    await this.SaveAsync();
                    return (TR)KeyValue;
                }
            }
            catch (Exception)
            {
                return default;
            }
        }

        public virtual int Save()
        {
            this.ToSql();
            return this.Context.InsertOrUpdateOrDeleteToSave();
        }

        public virtual Task<int> SaveAsync()
        {
            this.ToSql();
            return this.Context.InsertOrUpdateOrDeleteToSaveAsync();
        }
    }
}
