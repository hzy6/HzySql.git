﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.Curd.Select
{
    using System.Linq.Expressions;
    using HzySql.Curd.Select;
    using HzySql.Interface;
    using HzySql.Models;




    public class QueryAchieve<T1> : QueryDataAchieve<T1>, IQuery<T1>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => this.Context.CreateFromSql<T1>();

        public virtual IQuery<T1> Where(Expression<Func<HzyTuple<T1>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1> OrderBy<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1> OrderByDesc<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1> GroupBy<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1> Having(Expression<Func<HzyTuple<T1>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1> Select<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, TJoin>(this.Context);
        }

        public IQuery<T1> LockWith(bool isLock = true)
        {
            this.Context.CreateLockWith(isLock);
            return this;
        }
    }

    #region Content



    public class QueryAchieve<T1, T2> : QueryDataAchieve<T1>, IQuery<T1, T2>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T2>();

        public virtual IQuery<T1, T2> Where(Expression<Func<HzyTuple<T1, T2>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2> Having(Expression<Func<HzyTuple<T1, T2>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2> Select<TR>(Expression<Func<HzyTuple<T1, T2>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3> : QueryDataAchieve<T1>, IQuery<T1, T2, T3>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T3>();

        public virtual IQuery<T1, T2, T3> Where(Expression<Func<HzyTuple<T1, T2, T3>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3> Having(Expression<Func<HzyTuple<T1, T2, T3>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T4>();

        public virtual IQuery<T1, T2, T3, T4> Where(Expression<Func<HzyTuple<T1, T2, T3, T4>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4> Having(Expression<Func<HzyTuple<T1, T2, T3, T4>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T5>();

        public virtual IQuery<T1, T2, T3, T4, T5> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T6>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T7>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T8>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T9>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T10>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T11>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T12>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T13>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T14>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T15>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TJoin> Join<TJoin>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TJoin>, bool>> Exp, JoinType? _JoinType = null)
        {
            if (_JoinType == null) _JoinType = HzySqlExtend.DefaultJoinType;
            this.Context.CreateJoinSql(Exp, (JoinType)_JoinType);
            return new QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TJoin>(this.Context);
        }

    }



    public class QueryAchieve<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> : QueryDataAchieve<T1>, IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>
    {
        public QueryAchieve(ISqlContext Context) : base(Context) => ModelSet.GetOrCacheFieldInfo<T16>();

        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> Where(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateWhereSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> OrderBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> OrderByDesc<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateOrderByDescSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> GroupBy<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateGroupBySql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> Having(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>, bool>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateHavingSql(Exp);
            return this;
        }
        public virtual IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> Select<TR>(Expression<Func<HzyTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>, TR>> Exp, bool IF = true)
        {
            if (IF) this.Context.CreateSelectSql(Exp);
            return this;
        }

    }

    #endregion














}
