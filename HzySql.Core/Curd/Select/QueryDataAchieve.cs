﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.Curd.Select
{
    using HzySql;
    using HzySql.Curd.Select;
    using HzySql.Interface;
    using HzySql.Models;
    using System.Data;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class QueryDataAchieve : Base, IQueryData
    {
        public QueryDataAchieve(ISqlContext Context) : base(Context) { }

        public virtual ISqlContext ToSqlContext()
        {
            this.Context.ToSql();
            return this.Context;
        }
        public virtual IQueryData ToSqlContext(out ISqlContext iSqlContext)
        {
            iSqlContext = Context;
            return this;
        }
        public virtual string ToSql()
        {
            this.Context.ToSql();
            return this.Context.ToSqlString();
        }
        public virtual IQueryData ToSql(out string sqlCode)
        {
            sqlCode = this.ToSql();
            return this;
        }
        public virtual ISqlContext Union(ISqlContext Join)
        {
            this.Context.CreateUnion(Join, "UNION");
            return this.Context;
        }
        public virtual ISqlContext UnionAll(ISqlContext Join)
        {
            this.Context.CreateUnion(Join, "UNION ALL");
            return this.Context;
        }

        #region 同步
        public virtual int Count() => this.Context.ToCount();
        public virtual TR First<TR>()
        {
            this.Context.CreateTopSql(1);
            var res = this.Context.ToFirst<TR>();

            if (HzySqlExtend.ReturnInstance && res == null)
                return Utility.CreateInstance<TR>();

            return res;
        }
        public virtual List<TR> ToList<TR>() => this.Context.ToList<TR>().ToList();
        public virtual DataTable ToTable() => this.Context.ToTable();
        public virtual Dictionary<string, object> ToDictionary() => this.Context.ToDictionary();
        public virtual List<Dictionary<string, object>> ToDictionaryList() => this.Context.ToDictionaryList();
        #endregion

        #region 异步
        public virtual Task<int> CountAsync() => this.Context.ToCountAsync();
        public virtual async Task<TR> FirstAsync<TR>()
        {
            this.Context.CreateTopSql(1);
            var res = await this.Context.ToFirstAsync<TR>();

            if (HzySqlExtend.ReturnInstance && res == null)
                return Utility.CreateInstance<TR>();

            return res;
        }
        public virtual async Task<List<TR>> ToListAsync<TR>() => (await this.Context.ToListAsync<TR>()).ToList();
        public virtual Task<DataTable> ToTableAsync() => this.Context.ToTableAsync();
        public virtual Task<Dictionary<string, object>> ToDictionaryAsync() => this.Context.ToDictionaryAsync();
        public virtual Task<List<Dictionary<string, object>>> ToDictionaryListAsync() => this.Context.ToDictionaryListAsync();
        #endregion
    }

    public class QueryDataAchieve<T1> : QueryDataAchieve, IQueryData<T1>
    {
        public QueryDataAchieve(ISqlContext Context) : base(Context) { }

        public virtual new IQueryData<T1> ToSqlContext(out ISqlContext iSqlContext)
        {
            this.Context.ToSql();
            iSqlContext = Context;
            return this;
        }
        public virtual new IQueryData<T1> ToSql(out string sqlCode)
        {
            sqlCode = this.ToSql();
            return this;
        }
        public virtual IQueryData<T1> Top(int Top)
        {
            this.Context.CreateTopSql(Top);
            return this;
        }
        public virtual IQueryData<T1> Distinct()
        {
            this.Context.CreateDistinctSql();
            return this;
        }

        #region 分页
        public virtual IQueryData<T1> TakePage(int Page, int Rows)
        {
            this.Context.CreateTakePageSql(Page, Rows);
            return this;
        }
        public virtual IQueryData<T1> TakePage(int Page, int Rows, out int Count)
        {
            Count = this.Context.ToCount();
            this.Context.CreateTakePageSql(Page, Rows);
            return this;
        }
        #endregion

        #region 同步
        public virtual T1 First() => this.First<T1>();
        public virtual List<T1> ToList() => this.ToList<T1>();
        public virtual TR Max<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp) => this.Context.ToMax<TR>(Exp);
        public virtual TR Min<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp) => this.Context.ToMin<TR>(Exp);
        public virtual TR Sum<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp) => this.Context.ToSum<TR>(Exp);
        #endregion

        #region 异步
        public virtual Task<T1> FirstAsync() => this.FirstAsync<T1>();
        public virtual Task<List<T1>> ToListAsync() => this.ToListAsync<T1>();
        public virtual Task<TR> MaxAsync<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp) => this.Context.ToMaxAsync<TR>(Exp);
        public virtual Task<TR> MinAsync<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp) => this.Context.ToMinAsync<TR>(Exp);
        public virtual Task<TR> SumAsync<TR>(Expression<Func<HzyTuple<T1>, TR>> Exp) => this.Context.ToSumAsync<TR>(Exp);
        #endregion

    }




}
