﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.Curd
{
    using HzySql.Interface;

    public class Base
    {
        protected ISqlContext Context { get; set; }

        public Base(ISqlContext Context) => this.Context = Context;

    }
}
