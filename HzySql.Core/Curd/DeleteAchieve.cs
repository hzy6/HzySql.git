﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Core.Curd
{
    using HzySql.Curd;
    using HzySql.Interface;
    using HzySql.Models;
    using System.Threading.Tasks;

    public class DeleteAchieve<T> : InsertOrUpdateOrDelete<T>, IDelete<T>
        where T : class, new()
    {
        public DeleteAchieve(ISqlContext Context) : base(Context) { }

        public virtual IDelete<T> Where(Expression<Func<HzyTuple<T>, bool>> Where, bool IF)
        {
            if (Where == null || !IF) return this;
            this.Context.CreateWhereSql(Where);
            return this;
        }

        public virtual string ToSql()
        {
            this.Context.CreateDeleteSql(this.TableName);
            return this.Context.ToSqlString();
        }

        public virtual IDelete<T> ToSql(out string sqlCode)
        {
            sqlCode = this.ToSql();
            return this;
        }

        public virtual IDelete<T> ToSqlContext(out ISqlContext iSqlContext)
        {
            this.ToSql();
            iSqlContext = this.Context;
            return this;
        }

        public virtual ISqlContext ToSqlContext()
        {
            this.ToSql();
            return this.Context;
        }

        public virtual void ToSqlContext(List<ISqlContext> iSqlContexts) => iSqlContexts.Add(this.ToSqlContext());

        public virtual int Save()
        {
            this.ToSql();
            return this.Context.InsertOrUpdateOrDeleteToSave();
        }

        public virtual Task<int> SaveAsync()
        {
            this.ToSql();
            return this.Context.InsertOrUpdateOrDeleteToSaveAsync();
        }
    }
}
