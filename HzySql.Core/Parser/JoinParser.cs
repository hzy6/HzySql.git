﻿namespace HzySql.Core.Parser
{
    //
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using HzySql.Models;

    public class JoinParser
    {
        private SqlContextBase Context { get; set; }

        public JoinParser(SqlContextBase Context) => this.Context = Context;

        public void Create(LambdaExpression _LambdaExpression, JoinType _JoinType)
        {
            PropertyInfo propertyInfo = null;
            foreach (var item in _LambdaExpression.Parameters)
            {
                var _TypeInfo = item.Type as TypeInfo;
                propertyInfo = _TypeInfo.GetProperties().OrderByDescending(w => w.Name).ToList()[0];
            }

            var _Alias = this.Context.Symbol(propertyInfo.Name);
            var _TabName = this.Context.Symbol(propertyInfo.PropertyType.Name);

            this.Context.Join.Append($"\r\n{_JoinType.ToString().Replace("_", " ")} {_TabName} AS {_Alias}");

            var NewContext = this.Context.NewContextCall.Invoke();
            NewContext.CreateWhereSql(_LambdaExpression, "ON");

            this.Context.Join.Append(NewContext.Where);
            this.Context.Parameter = NewContext.Parameter;
            this.Context.Alias[_Alias] = _TabName;
        }





    }
}
