﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HzySql.Core.Parser
{
    using HzySql.Models;

    public class CaseParser : ExpressionVisitor
    {
        private List<MethodCallExpression> MethodCalls = new List<MethodCallExpression>();

        private SqlContextBase Context { get; set; }

        public CaseParser(SqlContextBase Context) => this.Context = Context;

        public override Expression Visit(Expression node) => base.Visit(node);

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            this.MethodCalls.Add(node);
            return this.Visit(node.Object);
        }

        public virtual StringBuilder AsSql()
        {
            StringBuilder Code = new StringBuilder();
            for (int i = MethodCalls.Count - 1; i >= 0; i--)
            {
                var item = MethodCalls[i];
                var Name = item.Method.Name;

                if (Name == nameof(HzySqlExtend.Case))
                {
                    Code.Append("CASE");
                }
                else if (Name == nameof(CaseAchieve.When))
                {
                    var _Arguments1 = item.Arguments[0];
                    var _Arguments2 = item.Arguments[1];

                    var WhereContext = this.Context.NewContextCall.Invoke();
                    WhereContext.Parameter = this.Context.Parameter;

                    var _WhereParser = new WhereParser(WhereContext);
                    _WhereParser.Visit(_Arguments1);

                    Context.Parameter = WhereContext.Parameter;
                    Code.Append($" WHEN{WhereContext.Where} THEN {_Arguments2}");
                }
                else if (Name == nameof(CaseAchieve.Else))
                {
                    var _Arguments1 = item.Arguments[0];
                    Code.Append($" ELSE {_Arguments1}");
                }
                else if (Name == nameof(CaseAchieve.End))
                {
                    var _Arguments1 = item.Arguments[0].ToString();
                    var EndName = (string.IsNullOrWhiteSpace(_Arguments1) ? "" : _Arguments1);
                    Code.Append($" END {(string.IsNullOrWhiteSpace(EndName) ? "" : this.Context.Symbol(EndName))}".Replace("\"", ""));
                }
            }

            return Code.Replace("\"", "'");
        }


    }
}
