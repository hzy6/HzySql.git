﻿using System.Collections.Generic;

namespace HzySql.Core.Parser
{
    using System.Linq;
    using System.Linq.Expressions;
    using HzySql;
    using HzySql.Models;

    /// <summary>
    /// groupby 解析
    /// </summary>
    public class GroupByParser : ExpressionVisitor
    {
        private SqlContextBase Context { get; set; }

        public GroupByParser(SqlContextBase Context) => this.Context = Context;

        public override Expression Visit(Expression node)
        {
            return base.Visit(node);
        }

        protected override Expression VisitNew(NewExpression node)
        {
            var _Arguments = node.Arguments;
            var _Members = node.Members;

            foreach (var item in _Arguments)
            {
                var _CharCount = item.ToString().Count(w => w == '.');
                if (item is MemberExpression & _CharCount == 2)
                {
                    var _MemberExpression = (MemberExpression)item;
                    var _Expression = _MemberExpression.Expression;
                    var _Tab = (MemberExpression)_Expression;
                    var _MemberName = this.Context.Symbol(_MemberExpression.Member.Name);
                    //别名
                    var _TabName = this.AddAlias(_Tab.Member.Name);
                    this.AddCode($"{_TabName}{_MemberName}");
                }
                else
                {
                    this.Visit(item);
                }
            }

            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            var _CharCount = node.ToString().Count(w => w == '.');
            if (node.Expression is MemberExpression & _CharCount == 2)
            {
                var _Expression = node.Expression;
                var _Tab = (MemberExpression)_Expression;
                var _MemberName = this.Context.Symbol(node.Member.Name);
                var _TabName = this.AddAlias(_Tab.Member.Name);
                this.AddCode($"{_TabName}{_MemberName}");
            }
            return node;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            this.AddCode(node.Value);
            return node;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var _MethodName = node.Method.Name;

            if (_MethodName == nameof(HzyTuple.HzySql))
            {
                var _Arguments1 = node.Arguments[0];
                //var _Arguments2 = node.Arguments[1];

                this.AddCode(Utility.Eval(_Arguments1));
            }
            else
            {
                this.AddCode(Utility.Eval(node));
            }
            return node;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            this.AddCode(Utility.Eval(node));
            return node;
        }

        private void AddCode(object Codes)
        {
            if (Codes == null)
                throw new HzySqlException(" GROUP BY 参数不能为空字符串或者Null ");
            //记录 Order By
            if (string.IsNullOrEmpty(this.Context.GroupBy.ToString()))
            {
                this.Context.GroupBy.Append(Codes);
            }
            else
            {
                this.Context.GroupBy.Append($",{Codes}");
            }
        }

        /// <summary>
        /// 追加别名
        /// </summary>
        private string AddAlias(string Alias)
        {
            if (this.Context.IsAlias)
                return $"{this.Context.Symbol(Alias)}.";
            return string.Empty;
        }

    }
}
