﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzySql.Core.Parser
{
    using System.Linq.Expressions;
    using HzySql.Models;
    
    public class Max_Min_Sum_Parser: ExpressionVisitor
    {
        private SqlContextBase Context { get; set; }
        protected string FunctionName { get; set; }

        public Max_Min_Sum_Parser(SqlContextBase Context, string functionName)
        {
            this.Context = Context;
            this.FunctionName = functionName;
        }

        public override Expression Visit(Expression node)
        {
            if (node == null) throw new HzySqlException($"函数[{FunctionName}]该语法无法解析!");
            return base.Visit(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            var _CharCount = node.ToString().Count(w => w == '.');
            if (node.Expression is MemberExpression & _CharCount == 2)
            {
                var _Expression = node.Expression;
                var _Tab = (MemberExpression)_Expression;
                var _MemberName = this.Context.Symbol(node.Member.Name);
                var _TabName = this.AddAlias(_Tab.Member.Name);

                this.AddCode($" {FunctionName}({_TabName + _MemberName}) ");
            }
            return node;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var _MethodName = node.Method.Name;

            if (_MethodName == nameof(HzyTuple.HzySql))
            {
                var _Arguments1 = node.Arguments[0];
                //var _Arguments2 = node.Arguments[1];

                this.AddCode($" {FunctionName}({Utility.Eval(_Arguments1)}) ");
            }
            return node;
        }

        private void AddCode(string Codes) => this.Context.Cols.Clear().Append(Codes);

        /// <summary>
        /// 追加别名
        /// </summary>
        private string AddAlias(string Alias)
        {
            if (this.Context.IsAlias)
                return $"{this.Context.Symbol(Alias)}.";
            return string.Empty;
        }


    }


}
