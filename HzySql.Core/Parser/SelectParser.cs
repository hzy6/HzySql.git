﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzySql.Core.Parser
{
    //
    using System.Reflection;
    using System.Linq;
    using System.Linq.Expressions;
    using HzySql;
    using HzySql.Models;

    /// <summary>
    /// select解析
    /// </summary>
    public class SelectParser : ExpressionVisitor
    {
        private SqlContextBase Context { get; set; }

        public SelectParser(SqlContextBase Context) => this.Context = Context;

        public override Expression Visit(Expression node)
        {
            return base.Visit(node);
        }

        protected override Expression VisitNew(NewExpression node)
        {
            var _Arguments = node.Arguments;
            var _Members = node.Members;

            foreach (var item in _Arguments)
            {
                var _CharCount = item.ToString().Count(w => w == '.');
                //别名
                var _Name = this.Context.Symbol(_Members[_Arguments.IndexOf(item)].Name);

                if (item is MemberExpression & _CharCount == 2)
                {
                    var _MemberExpression = (MemberExpression)item;
                    var _Expression = _MemberExpression.Expression;
                    var _Tab = (MemberExpression)_Expression;
                    var _MemberName = this.Context.Symbol(_MemberExpression.Member.Name);
                    var _TabName = this.AddAlias(_Tab.Member.Name);
                    var _AS = (_Name == _MemberName) ? string.Empty : $" AS {_Name}";
                    this.AddCode($"{_TabName}{_MemberName}{_AS}");
                }
                else if (item is ConstantExpression & !_Name.ToLower().Contains(nameof(HzyTuple.HzySql).ToLower()))
                {
                    var _ConstantExpression = (ConstantExpression)item;
                    var _Value = _ConstantExpression.Value;
                    this.AddCode($"{(string.IsNullOrWhiteSpace(_Value.ToString()) ? "NULL" : _Value)} {_Name}");
                }
                else if (item is MethodCallExpression & !_Name.ToLower().Contains(nameof(HzyTuple.HzySql).ToLower()))
                {
                    var _node = item as MethodCallExpression;
                    var _Value = Utility.Eval(_node.Arguments[0]);
                    this.AddCode(_Value + $" AS {((_Value.ToString() != _Name) ? _Name : "")}");
                }
                else
                {
                    this.Visit(item);
                }
            }

            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            var _CharCount = node.ToString().Count(w => w == '.');
            if (node is MemberExpression & _CharCount == 1)
            {
                var _Column = new List<string>();
                var _Member = node.Member as PropertyInfo;
                this.AnalysisParameterExpression(_Member.PropertyType, _Column);
                this.AddCode(string.Join(",", _Column));
            }
            else if (node.Expression is MemberExpression & _CharCount == 2)
            {
                var _Expression = node.Expression;
                var _Tab = (MemberExpression)_Expression;
                var _MemberName = this.Context.Symbol(node.Member.Name);
                var _TabName = this.AddAlias(_Tab.Member.Name);
                this.AddCode($"{_TabName}{_MemberName}");
            }
            return node;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            this.AddCode(node.Value);
            return node;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var _MethodName = node.Method.Name;

            if (_MethodName == nameof(HzyTuple.HzySql))
            {
                var _Arguments1 = node.Arguments[0];
                //var _Arguments2 = node.Arguments[1];

                this.AddCode(Utility.Eval(_Arguments1));
            }
            else if (node.Type == typeof(CaseAchieve))
            {
                var _CaseParser = new CaseParser(this.Context);
                _CaseParser.Visit(node);
                var Code = _CaseParser.AsSql();
                this.AddCode(Code);
            }
            else
            {
                this.AddCode(Utility.Eval(node));
            }
            return node;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            this.AddCode(Utility.Eval(node));
            return node;
        }



        /// <summary>
        /// 替换 列
        /// </summary>
        /// <param name="_Sql"></param>
        /// <param name="Codes"></param>
        private void AddCode(object Codes)
        {
            if (Codes == null) return;

            if (string.IsNullOrEmpty(Codes.ToString()) || Codes.ToString() == ("*")) return;
            var _Code_Column = this.Context.Cols.ToString().Trim();
            if (string.IsNullOrEmpty(_Code_Column))
            {
                this.Context.Cols.Append(Codes);
            }
            else if (_Code_Column == "*")
            {
                this.Context.Cols.Clear().Append(Codes);
            }
            else
            {
                this.Context.Cols.Append($",{Codes}");
            }
            //_Sql.Code_Column.Clear().Append(Codes);//记录列
        }

        /// <summary>
        /// 追加别名
        /// </summary>
        private string AddAlias(string Alias)
        {
            if (this.Context.IsAlias)
                return $"{this.Context.Symbol(Alias)}.";
            return string.Empty;
        }

        private void AnalysisParameterExpression(Type _Type, List<string> _Column)
        {
            var _TableInfo = ModelSet.GetOrCacheFieldInfo(_Type);
            var TableName = _TableInfo.FirstOrDefault().TableName;

            var _TabName = this.Context.Alias.FirstOrDefault(w => w.Value.ToString() == this.Context.Symbol(TableName));

            foreach (var item in _TableInfo)
            {
                //检测有无忽略字段
                if (_TableInfo.Any(w => w.NotMapped && w.ColumnName == item.ColumnName))
                    continue;

                if (this.Context.IsAlias)
                    _Column.Add(_TabName.Key + "." + this.Context.Symbol(item.ColumnName));
                else
                    _Column.Add(this.Context.Symbol(item.ColumnName));
            }
        }



    }
}