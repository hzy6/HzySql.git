﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.Parser
{
    using System.Linq;
    using System.Linq.Expressions;
    using System.Collections;
    using HzySql;
    using HzySql.Models;
    using HzySql.Interface;

    public class WhereParser : ExpressionVisitor
    {
        private ISqlContext Context { get; set; }

        public WhereParser(ISqlContext Context) => this.Context = Context;

        /// <summary>
        /// 入口函数
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public override Expression Visit(Expression node) => base.Visit(node);

        /// <summary>
        /// 二元树
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitBinary(BinaryExpression node)
        {
            //检查括号
            this.CheckBrackets(node.ToString(), () =>
            {
                //左边
                this.CheckBrackets(node.Left.ToString(), () => this.Visit(node.Left));

                this.Context.Where.Append($" {this.GetOperatorStr(node.NodeType)}");

                //右边
                this.CheckBrackets(node.Right.ToString(), () => this.Visit(node.Right));
            });

            return node;
        }

        /// <summary>
        /// 属性或者字段
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression == null)
            {
                this.Eval(node);
                return node;
            }

            //字段名
            var _MemberName = this.Context.Symbol(node.Member.Name);
            var _MemberStr = node.ToString();
            if (node.Expression is MemberExpression & _MemberStr.Count(w => w == '.') == 2)
            {
                if (this.Context.IsAlias)
                {
                    //取上一个member为参数名称 别名
                    var _Alias = this.Context.Symbol((node.Expression as MemberExpression).Member.Name);
                    this.Context.Where.Append($" {_Alias}.{_MemberName}");
                }
                else
                {
                    this.Context.Where.Append($" {_MemberName}");
                }
            }
            else if (node.Expression.GetType().Name == "TypedParameterExpression")
            {
                this.Context.Where.Append($" {_MemberName}");
            }
            else
            {
                this.Eval(node);
            }

            return node;
        }

        /// <summary>
        /// 常量值表达式
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitConstant(ConstantExpression node)
        {
            this.AddParameter(node.Value);
            return node;
        }

        /// <summary>
        /// 一元运算符
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Not & node.Operand is MethodCallExpression)
            {
                this.HandleVisitMethodCall(node.Operand as MethodCallExpression, true);
                return node;
            }

            return base.Visit(node.Operand);
        }

        //protected override Expression VisitNew(NewExpression node)
        //{
        //    return base.Visit(node);
        //}

        //protected override Expression VisitNewArray(NewArrayExpression node)
        //{
        //    return base.Visit(node);
        //}

        //protected override Expression VisitListInit(ListInitExpression node)
        //{
        //    return base.Visit(node);
        //}

        /// <summary>
        /// 表示对静态方法或实例方法的调用
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            this.HandleVisitMethodCall(node, false);
            return node;
        }

        #region MethodCall

        /// <summary>
        /// MethodCallExpression 操作
        /// </summary>
        /// <param name="node"></param>
        /// <param name="IsNot"></param>
        private void HandleVisitMethodCall(MethodCallExpression node, bool IsNot)
        {
            if (Enum.TryParse(node.Method.Name, out MethodName_Enum _MethodEnum))
            {
                switch (_MethodEnum)
                {
                    case MethodName_Enum.StartsWith:
                        this.StartsWith(node, "LIKE");
                        break;
                    case MethodName_Enum.Contains:
                        var _MemberExpression = node.Object as MemberExpression;
                        if (_MemberExpression != null & _MemberExpression?.Type.Name == "List`1")
                            this.InOrNotIn(node, IsNot ? "NOT IN" : "IN");
                        else
                            this.Contains(node, "LIKE");
                        break;
                    case MethodName_Enum.EndsWith:
                        this.EndsWith(node, "LIKE");
                        break;
                    case MethodName_Enum.Equals:
                        this.Equals(node, IsNot ? "<>" : "=");
                        break;
                    case MethodName_Enum.Like:
                        this.Like(node, "LIKE");
                        break;
                    case MethodName_Enum.LikeStart:
                        this.LikeStart(node, "LIKE");
                        break;
                    case MethodName_Enum.LikeEnd:
                        this.LikeEnd(node, "LIKE");
                        break;
                    case MethodName_Enum.HzySql:
                        this.SqlStr(node);
                        break;
                    case MethodName_Enum.In:
                        this.CustomInOrNotIn(node, "IN");
                        break;
                    case MethodName_Enum.NotIn:
                        this.CustomInOrNotIn(node, "NOT IN");
                        break;
                    default:
                        this.Eval(node);
                        break;
                }
            }
            else
            {
                this.Eval(node);
            }
        }

        private void Like(MethodCallExpression node, string Symbol)
        {
            var _Arguments1 = node.Arguments[0];
            var _Argument2 = node.Arguments[1];

            this.Visit(_Arguments1);
            this.Context.Where.Append($" {Symbol}");

            this.Context.Where.Append(" '%'+");
            this.Visit(_Argument2);
            this.Context.Where.Append(" + '%'");
        }

        private void LikeStart(MethodCallExpression node, string Symbol)
        {
            var _Arguments1 = node.Arguments[0];
            var _Argument2 = node.Arguments[1];

            this.Visit(_Arguments1);
            this.Context.Where.Append($" {Symbol}");

            this.Visit(_Argument2);
            this.Context.Where.Append(" + '%'");
        }

        private void LikeEnd(MethodCallExpression node, string Symbol)
        {
            var _Arguments1 = node.Arguments[0];
            var _Argument2 = node.Arguments[1];

            this.Visit(_Arguments1);
            this.Context.Where.Append($" {Symbol}");

            this.Context.Where.Append(" '%'+");
            this.Visit(_Argument2);
        }

        private void StartsWith(MethodCallExpression node, string Symbol)
        {
            this.Visit(node.Object);
            this.Context.Where.Append($" {Symbol}");
            this.Visit(node.Arguments[0]);
            this.Context.Where.Append(" + '%'");
        }

        private void EndsWith(MethodCallExpression node, string Symbol)
        {
            this.Visit(node.Object);
            this.Context.Where.Append($" {Symbol}");
            this.Context.Where.Append(" '%'+");
            this.Visit(node.Arguments[0]);
        }

        private void Contains(MethodCallExpression node, string Symbol)
        {
            this.Visit(node.Object);
            this.Context.Where.Append($" {Symbol}");
            var Arguments1 = node.Arguments[0];
            //如果
            this.Context.Where.Append(" '%'+");
            this.Visit(Arguments1);
            this.Context.Where.Append(" + '%'");
        }

        private void InOrNotIn(MethodCallExpression node, string Symbol)
        {
            var Arguments1 = node.Arguments[0];
            this.Visit(Arguments1);
            this.Context.Where.Append($" {Symbol} (");
            this.Visit(node.Object);
            this.Context.Where.Append(" ) ");
        }

        private void Equals(MethodCallExpression node, string Symbol)
        {
            this.Visit(node.Object);
            this.Context.Where.Append($" {Symbol}");
            this.Visit(node.Arguments[0]);
        }

        private void SqlStr(MethodCallExpression node)
        {
            var _Argument1 = node.Arguments[0];
            this.Context.Where.Append($" {Utility.Eval(_Argument1)}");
        }

        private void CustomInOrNotIn(MethodCallExpression node, string Symbol)
        {
            var _Argument1 = node.Arguments[0];
            var _Argument2 = node.Arguments[1];

            if (_Argument2.Type.BaseType == typeof(Array))
            {
                this.Visit(_Argument1);
                this.Context.Where.Append($" {Symbol} (");
                this.Visit(_Argument2);
                this.Context.Where.Append(" )");
            }
            else if (_Argument2.Type == typeof(ISqlContext))
            {
                var SqlContext = (ISqlContext)Utility.Eval(_Argument2);
                SqlContext = this.Context.CreateUnion(SqlContext);
                //
                this.Visit(_Argument1);
                this.Context.Where.Append($" {Symbol} (");
                this.Context.Where.Append(SqlContext.Code);
                this.Context.Where.Append(" )");
            }
        }

        #endregion

        #region 辅助函数

        /// <summary>
        /// 运算符
        /// </summary>
        /// <param name="_ExpressionType"></param>
        /// <returns></returns>
        private string GetOperatorStr(ExpressionType _ExpressionType)
        {
            switch (_ExpressionType)
            {
                case ExpressionType.Or:
                case ExpressionType.OrElse: return " OR";
                case ExpressionType.And:
                case ExpressionType.AndAlso: return " AND";
                case ExpressionType.GreaterThan: return ">";
                case ExpressionType.GreaterThanOrEqual: return ">=";
                case ExpressionType.LessThan: return "<";
                case ExpressionType.LessThanOrEqual: return "<=";
                case ExpressionType.Equal: return "=";
                case ExpressionType.NotEqual: return "<>";
                case ExpressionType.Add: return "+";
                case ExpressionType.Subtract: return "-";
                case ExpressionType.Multiply: return "*";
                case ExpressionType.Divide: return "/";
                case ExpressionType.Modulo: return "%";
                default: throw new HzySqlException("无法解析的表达式！");
            }
        }

        /// <summary>
        /// 检查是否有 括号
        /// </summary>
        /// <param name="_Action"></param>
        private void CheckBrackets(string _Str, Action _Action)
        {
            //检查是否有括号
            var _AddBrackets = !string.IsNullOrEmpty(_Str) && _Str.Length > 5 && _Str.StartsWith("((") && _Str.EndsWith("))");

            if (_AddBrackets) this.Context.Where.Append(" (");

            _Action();

            if (_AddBrackets) this.Context.Where.Append(" )");
        }

        /// <summary>
        /// 追加参数
        /// </summary>
        /// <param name="Value"></param>
        private void AddParameter(object Value)
        {
            if (Value == null || Value == DBNull.Value)
            {
                this.Context.Where.Append(" NULL");
                this.Context.Where.Replace(" = NULL", " IS NULL ");
                this.Context.Where.Replace(" <> NULL", " IS NOT NULL ");
                return;
            }

            if (Value.GetType() == typeof(bool))
            {
                if ((bool)Value)
                    this.Context.Where.Append(" 1=1");
                else
                    this.Context.Where.Append(" 1=2");
                return;
            }

            string Name = $"{this.Context.ParametricSymbols}P{this.Context.Parameter.Count}";
            this.Context.Parameter.Add(new DataParameter(Name, Value));
            this.Context.Where.Append($" {Name}");
        }

        /// <summary>
        /// 参数值处理
        /// </summary>
        /// <param name="_Expression"></param>
        private void Eval(Expression _Expression)
        {
            UnaryExpression cast = Expression.Convert(_Expression, typeof(object));
            var _ObjectValue = Expression.Lambda<Func<object>>(cast).Compile().Invoke();
            var type = _ObjectValue.GetType();

            if (type.BaseType == typeof(Array) || type.Name == "List`1")
            {
                var list = _ObjectValue as IEnumerable;
                var index = 0;
                foreach (var item in list)
                {
                    this.AddParameter(item);
                    index = this.Context.Where.Length;
                    this.Context.Where.Append(",");
                }
                this.Context.Where.Remove(index, 1);
            }
            else
            {
                this.AddParameter(_ObjectValue);
            }
        }

        #endregion
    }

    /// <summary>
    /// 函数集合
    /// </summary>
    public enum MethodName_Enum
    {
        Like,
        LikeStart,
        LikeEnd,
        StartsWith,
        Contains,
        EndsWith,
        Equals,
        HzySql,
        In,
        NotIn,
    }

}
