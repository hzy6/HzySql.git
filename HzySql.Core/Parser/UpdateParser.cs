﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.Parser
{
    //
    using System.Linq.Expressions;
    using HzySql;
    
    public class UpdateParser : ExpressionVisitor
    {
        public StringBuilder _StringBuilder { get; set; }= new StringBuilder();
        protected override Expression VisitBinary(BinaryExpression node)
        {
            this.Visit(node.Left);
            this._StringBuilder.Append(this.GetOperatorStr(node.NodeType));
            this.Visit(node.Right);
            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            this._StringBuilder.Append(node.Member.Name);
            return node;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            this._StringBuilder.Append(node.Value);
            return node;
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            return base.Visit(node.Operand);
        }

        private string GetOperatorStr(ExpressionType _ExpressionType)
        {
            switch (_ExpressionType)
            {
                case ExpressionType.Equal: return " = ";
                case ExpressionType.Add: return " + ";
                case ExpressionType.Subtract: return " - ";
                case ExpressionType.Multiply: return " * ";
                case ExpressionType.Divide: return " / ";
                case ExpressionType.Modulo: return " % ";
                default: throw new HzySqlException("无法解析的表达式！");
            }
        }

    }
}
