﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.SqlServer
{
    /// <summary>
    /// 分页模式
    /// </summary>
    public enum PagingMode
    {
        /// <summary>
        /// RowNumber 分页 适用于 2012 一下数据库
        /// </summary>
        ROW_NUMBER,
        /// <summary>
        /// sqlserver 2012 以上支持
        /// </summary>
        OFFSET

    }
}
