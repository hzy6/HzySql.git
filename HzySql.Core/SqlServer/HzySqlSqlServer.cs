﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.SqlServer
{
    using HzySql;
    using HzySql.Interface;
    using HzySql.Core;

    /// <summary>
    /// SqlServer 上下文
    /// </summary>
    public class HzySqlSqlServer : HzySqlBase, IHzySql
    {
        protected PagingMode pagingMode { get; set; } = PagingMode.OFFSET;

        public HzySqlSqlServer() { }

        public HzySqlSqlServer(PagingMode pagingMode)
        {
            this.pagingMode = pagingMode;
        }

        public override ISqlContext Context => new SqlContextSqlServer(this.pagingMode, this.AdoProvider);


    }
}
