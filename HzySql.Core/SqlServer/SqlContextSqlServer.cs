﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Core.SqlServer
{
    using System.Linq;
    using System.Threading.Tasks;
    using HzySql;
    using HzySql.Interface;
    using HzySql.Models;

    public class SqlContextSqlServer : SqlContextBase, ISqlContext
    {
        public SqlContextSqlServer(PagingMode pagingMode, IHzySqlAdoProvider adoProvider)
            : base(adoProvider)
        {
            this.PagingMode = pagingMode;
            this.DataBaseType = DataBaseType.SqlServer;
            this.NewContextCall = () => new SqlContextSqlServer(pagingMode, adoProvider);
        }

        public PagingMode PagingMode { get; set; } = PagingMode.OFFSET;

        /// <summary>
        /// 特殊符号
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public override string Symbol(string Name) => $"[{Name}]";

        /// <summary>
        /// 参数化特殊符号
        /// </summary>
        public override string ParametricSymbols => "@";

        #region Insert、Update、Delete、Select

        /// <summary>
        /// 转换为最终Sql
        /// </summary>
        /// <param name="Cols"></param>
        /// <param name="From"></param>
        /// <param name="Join"></param>
        /// <param name="Where"></param>
        /// <param name="GroupBy"></param>
        /// <param name="Having"></param>
        /// <param name="OrderBy"></param>
        /// <param name="TakePage"></param>
        /// <returns></returns>
        public override StringBuilder ToSql(StringBuilder Cols, StringBuilder From, StringBuilder Join, StringBuilder Where, StringBuilder GroupBy, StringBuilder Having, StringBuilder OrderBy, StringBuilder TakePage)
        {
            var sb = new StringBuilder();
            //ROWS FETCH 分页
            var orderByString = OrderBy.ToString();
            if (PagingMode == PagingMode.OFFSET && TakePage.Length > 0) orderByString += TakePage;
            //组装
            sb.Append($"SELECT {Cols}\r\nFROM {From}");
            if (Join.Length > 0) sb.Append(Join);
            if (Where.Length > 0) sb.Append($"\r\nWHERE 1=1 {Where}");
            if (GroupBy.Length > 0) sb.Append($"\r\nGROUP BY {GroupBy}");
            if (Having.Length > 0) sb.Append($"\r\nHAVING {Having}");
            if (!string.IsNullOrWhiteSpace(orderByString)) sb.Append($"\r\nORDER BY {orderByString}");
            //ROW_NUMBER
            if (PagingMode == PagingMode.ROW_NUMBER && TakePage.Length > 0)
            {
                var takePageSql = TakePage.ToString().Replace("#SqlContent#", sb.ToString());
                sb.Clear().Append(takePageSql);
            }
            //lock with
            sb.Replace("<#LockWith#>", this.LockWith.ToString());
            return sb;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        public override void CreateTakePageSql(int Page, int Rows)
        {
            if (this.PagingMode == PagingMode.OFFSET)
            {
                if (this.OrderBy.Length == 0) this.OrderBy.Append("1");
                var _OFFSET = (Rows * (Page - 1));
                this.TakePage.Clear().Append($" OFFSET {_OFFSET} ROWS FETCH NEXT {Rows} ROWS ONLY");
            }
            else
            {
                var orderBySql = this.OrderBy.ToString();
                if (this.OrderBy.Length == 0) orderBySql = "(SELECT 1)";
                if (this.Cols.ToString().ToLower().Contains("DISTINCT".ToLower()))
                {
                    foreach (var item in this.Alias.Keys) orderBySql = orderBySql.Replace(item.ToString(), "TAB_ROWID");

                    this.TakePage.Clear().Append($@"
SELECT * FROM (
    SELECT *,ROW_NUMBER() OVER(ORDER BY {orderBySql}) AS ROWID FROM (
        #SqlContent#
    ) AS TAB_ROWID
) TAB_ROW_NUMBER WHERE TAB_ROW_NUMBER.ROWID BETWEEN {(Rows * (Page - 1)) + 1} AND {Page * Rows}
");
                }
                else
                {
                    this.Cols.Append($",ROW_NUMBER() OVER(ORDER BY {orderBySql}) AS ROWID");

                    this.TakePage.Clear().Append($@"
SELECT * FROM (
#SqlContent#
) AS TAB_ROWID
 WHERE TAB_ROWID.ROWID BETWEEN {(Rows * (Page - 1)) + 1} AND {Page * Rows}
");
                }
                //清空 Orderby
                this.OrderBy.Clear();
            }

            /*
    SELECT * FROM
    (
        SELECT
            ROW_NUMBER () OVER (

                ORDER BY
                    Designer_UserInfo.CreatTime DESC
            ) AS ROWID,
            Designer_UserInfo.ID,
            Designer_UserInfo.CreatTime,
            Designer_UserInfo.TrueName,
            Student_UserInfo.Age
        FROM
            Designer_UserInfo
        LEFT JOIN Student_UserInfo ON Designer_UserInfo.ID = Student_UserInfo.ID
    ) AS a
    WHERE ROWID BETWEEN 1 AND 3

    若把ROWID BETWEEN 1 AND 3改为 ROWID BETWEEN 3 AND 4则会返回最后两条，实际开发中变为 ROWID BETWEEN (pageindex-1)*pagesize+ 1 AND pageindex*pagesize即可 

     */
        }

        /// <summary>
        /// Top
        /// </summary>
        /// <param name="Top"></param>
        public override void CreateTopSql(int Top)
        {
            var _TOP = "TOP";
            var _DISTINCT = "DISTINCT";

            var _Codes = this.Cols.ToString().ToLower();
            if (!_Codes.Contains(_TOP.ToLower()))
            {
                if (_Codes.TrimStart().StartsWith(_DISTINCT.ToLower()))
                    this.Cols.Replace(_DISTINCT, $" {_DISTINCT} TOP ({Top}) ");
                else
                    this.Cols.Insert(0, $" TOP ({Top}) ");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isLock"></param>
        public override void CreateLockWith(bool isLock)
        {
            if (isLock)
            {
                //With(UpdLock, RowLock, NoWait)
                this.LockWith.Append("WITH(UpdLock, RowLock, NoWait)");
            }
        }

        #endregion

        #region 数据转换

        public override TR InsertToSave<TR>()
        {
            this.CheckAdoProviderNull();
            this.Code.Append("SELECT @@IDENTITY;");
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalar<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }
        public override Task<TR> InsertToSaveAsync<TR>()
        {
            this.CheckAdoProviderNull();
            this.Code.Append("SELECT @@IDENTITY;");
            HzySqlExtend.ExecuteSqlBeforeCall?.Invoke(this, this.ToSqlString());
            return this.AdoProvider.ExecuteScalarAsync<TR>(this.Code.ToString(), this.Parameter, this.AdoProvider.DbTransaction);
        }

        #endregion

        #region 获取用到的表集合

        public override List<string> GetTableNames()
        {
            var names = new List<string>();
            foreach (var item in this.Alias.Values)
            {
                var tabName = item.Replace("[", "");
                tabName = tabName.Replace("]", "");
                if (names.Any(w => w == tabName)) continue;
                names.Add(tabName);
            }
            return names;
        }

        #endregion


    }
}
