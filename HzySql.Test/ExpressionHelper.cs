﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HzySql.Test
{
    public class ExpressionHelper
    {
        /// <summary>
        /// 生成表达式树 例如：( w=> new [] {1,2,3}.Contains(w.Key) )
        /// </summary>
        /// <param name="name"></param>
        /// <param name="values"></param>
        /// <param name="expName"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public static Expression<Func<T, bool>> CreateContainsExpression<T, TKey>(string name, IEnumerable<TKey> values,
            string expName = "w")
        {
            var type = typeof(T);
            var parameter = Expression.Parameter(type, expName);

            var valuesParameter = Expression.Constant(values.ToList(), typeof(List<TKey>));
            var memberExpression = Expression.Property(parameter, name);
            var nameParameter = Expression.Convert(memberExpression, typeof(TKey));
            var body = Expression.Call(valuesParameter, "Contains", Array.Empty<Type>(), nameParameter);

            return Expression.Lambda<Func<T, bool>>(body, parameter);
        }

        /// <summary>
        /// 生成表达式树 例如：( w=> new [] {1,2,3}.Contains(w.Key) )
        /// </summary>
        /// <param name="name"></param>
        /// <param name="values"></param>
        /// <param name="expName"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Expression<Func<T, bool>> CreateContainsExpression<T>(string name, IEnumerable<object> values,
            string expName = "w")
            => CreateContainsExpression<T, Object>(name, values, expName);

        /// <summary>
        /// 生成表达式树 例如：( w=>w.Key==Guid.Empty )
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="expName"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public static Expression<Func<T, bool>> CreateEqualExpression<T, TKey>(string name, TKey value,
            string expName = "w")
        {
            //创建 Where Lambda表达式树
            var type = typeof(T);
            var parameter = Expression.Parameter(type, expName);
            
            var propertyParameter = Expression.Property(parameter, name);
            var nameParameter = Expression.Convert(propertyParameter, typeof(TKey));
            var valueParameter = Expression.Constant(value, typeof(TKey));

            var body = Expression.Equal(nameParameter, valueParameter);
            return Expression.Lambda<Func<T, bool>>(body, parameter);
        }

        /// <summary>
        /// 生成表达式树 例如：( w=>w.Key==Guid.Empty )
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="expName"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Expression<Func<T, bool>> CreateEqualExpression<T>(string name, object value,
            string expName = "w")
            => CreateEqualExpression<T, object>(name, value, expName);
    }
}