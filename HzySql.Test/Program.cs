﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HzySql.Test.Models;

namespace HzySql.Test
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var member = new Member();
            var guids = new Guid[] {Guid.Empty, Guid.NewGuid()};
            var id = Guid.NewGuid();
            var expWhere = ExpressionHelper.CreateContainsExpression<Member, Guid>(nameof(Member.Member_ID), guids);
            var expWhere2 =
                ExpressionHelper.CreateContainsExpression<Member>(nameof(Member.Member_ID),
                    guids.Select(w => (object) w));
            //
            var expWhere3 =
                ExpressionHelper.CreateEqualExpression<Member>(nameof(Member.Member_ID), id);

            // await new SqlServer().Run();
            //await new MySql().Run();

            Console.WriteLine("OK\r\n");
            //Console.ReadKey();
        }
    }
}