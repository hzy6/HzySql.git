﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Test.Models.Sys
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Sys_AppLog))]
    public class Sys_AppLog
    {
        [Key]
        public Guid AppLog_ID { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Api
        /// </summary>
        public string AppLog_Api { get; set; }

        /// <summary>
        /// Ip
        /// </summary>
        public string AppLog_IP { get; set; }

        public Guid AppLog_UserID { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public string AppLog_Parameter { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime AppLog_CreateTime { get; set; }


    }
}
