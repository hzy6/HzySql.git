﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HzySql.Test.Models.Sys
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Sys_Menu))]
    public class Sys_Menu
    {

        [Key]
        public Guid Menu_ID { get; set; } = Guid.NewGuid();

        /// <summary>
        /// 编号
        /// </summary>
        public string Menu_Num { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Menu_Name { get; set; }

        /// <summary>
        /// 菜单地址
        /// </summary>
        public string Menu_Url { get; set; }

        /// <summary>
        /// 路由地址
        /// </summary>
        public string Menu_RoutePath { get; set; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Menu_Icon { get; set; }

        /// <summary>
        /// 上级菜单
        /// </summary>
        public Guid Menu_ParentID { get; set; } = Guid.Empty;

        /// <summary>
        /// 是否显示=>1 是 2 否
        /// </summary>
        public int Menu_IsShow { get; set; } = 1;

        /// <summary>
        /// 创建时间
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Menu_CreateTime { get; set; }

    }
}
