﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HzySql.Test.Models
{
    using HzySql.Attributes;

    public class Member
    {
        [Key]
        public Guid Member_ID { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Member_Num { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        [HzySqlRemark("用户名称")]
        public string Member_Name { get; set; }

        /// <summary>
        /// 联系电话 => 我是显示名称 忽略文本
        /// </summary>
        public string Member_Phone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Member_Sex { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Member_Birthday { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Member_Photo { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        // [HzySqlForeignKey(typeof(Sys_User), nameof(Sys_User.User_ID))]
        public Guid? Member_UserID { get; set; }

        public string Member_Introduce { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? Member_CreateTime { get; set; }
    }
}
